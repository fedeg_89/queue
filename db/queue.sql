-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2020 a las 21:47:16
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `queue`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agents`
--

CREATE TABLE `agents` (
  `agent_id` int(5) NOT NULL,
  `client_id` int(3) NOT NULL,
  `agent_name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `agent_tel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `agent_mail` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `agents`
--

INSERT INTO `agents` (`agent_id`, `client_id`, `agent_name`, `agent_tel`, `agent_mail`) VALUES
(1, 1, 'Federico Gatti', '1188996655', 'fede@hamburgeseria.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `client_id` int(3) NOT NULL,
  `client_name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `client_address` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `client_tel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `client_mail` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `client_web` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `queue_domain` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `must_pay` tinyint(1) NOT NULL DEFAULT '0',
  `mercado_pago` tinyint(1) NOT NULL DEFAULT '0',
  `orders_timing` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `opens` time NOT NULL,
  `closes` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`client_id`, `client_name`, `client_address`, `client_tel`, `client_mail`, `client_web`, `queue_domain`, `must_pay`, `mercado_pago`, `orders_timing`, `opens`, `closes`) VALUES
(1, 'Mi Hamburgersería', 'Av. Directorio 1111', '1175116508', 'mi@hamburgueseria.com', 'www.mihamburgueseria.com', 'burger', 1, 0, 'manual', '18:00:00', '02:00:00'),
(2, 'Bronx', 'Av Directorio 710', '1100000000', 'bronx@ea.com', 'nup', 'bronx', 0, 0, 'auto', '19:30:00', '01:15:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options`
--

CREATE TABLE `options` (
  `client_id` int(3) NOT NULL,
  `option_id` int(3) NOT NULL,
  `option_group_id` int(3) NOT NULL,
  `option_group_name` varchar(15) NOT NULL,
  `option_name` varchar(15) NOT NULL,
  `option_price` decimal(6,2) DEFAULT NULL,
  `option_type` varchar(8) NOT NULL,
  `type_id` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `options`
--

INSERT INTO `options` (`client_id`, `option_id`, `option_group_id`, `option_group_name`, `option_name`, `option_price`, `option_type`, `type_id`) VALUES
(1, 1, 1, 'Pan', 'Pan común', NULL, 'radio', 0),
(1, 2, 1, 'Pan', 'Pan con sésamo', NULL, 'radio', 0),
(1, 3, 2, 'Sal', 'Con Sal', NULL, 'radio', 0),
(1, 4, 2, 'Sal', 'Sin Sal', NULL, 'radio', 0),
(1, 5, 3, 'Medallón', 'Simple', NULL, 'radio', 0),
(1, 6, 3, 'Medallón', 'Doble', '70.00', 'radio', 0),
(1, 7, 3, 'Medallón', 'Triple', '90.00', 'radio', 0),
(1, 8, 4, 'Extra', 'Jamón', '40.00', 'checkbox', 0),
(1, 9, 4, 'Extra', 'Queso', '40.00', 'checkbox', 0),
(1, 10, 4, 'Extra', 'Cheddar', '40.00', 'checkbox', 0),
(1, 11, 4, 'Extra', 'Cebolla', '40.00', 'checkbox', 0),
(1, 12, 4, 'Extra', 'Panceta', '40.00', 'checkbox', 0),
(1, 100, 100, 'Porción', 'Medianas', '70.00', 'radio', 1),
(1, 101, 100, 'Porción', 'Grandes', '120.00', 'radio', 1),
(1, 200, 200, 'Tamaño', 'Porrón', NULL, 'radio', 2),
(1, 201, 200, 'Tamaño', 'Jarra', NULL, 'radio', 2),
(1, 202, 201, 'Tipo', 'Rubia', NULL, 'radio', 2),
(1, 203, 201, 'Tipo', 'Negra', NULL, 'radio', 2),
(1, 204, 201, 'Tipo', 'Roja', NULL, 'radio', 2),
(1, 205, 202, 'Marca', 'Quilmes', NULL, 'radio', 2),
(1, 206, 202, 'Marca', 'Stella Artoise', NULL, 'radio', 2),
(1, 207, 202, 'Marca', 'Heineken', NULL, 'radio', 2),
(1, 208, 203, 'Marca', 'Coca-Cola', NULL, 'radio', 2),
(1, 209, 203, 'Marca', 'Fanta', NULL, 'radio', 2),
(1, 210, 203, 'Marca', 'Sprite', NULL, 'radio', 2),
(1, 211, 204, 'Gas', 'Con Gas', NULL, 'radio', 2),
(1, 212, 204, 'Gas', 'Sin Gas', NULL, 'radio', 2),
(1, 300, 300, 'Sabor', 'Vainilla', NULL, 'radio', 3),
(1, 301, 300, 'Sabor', 'Chocolate', NULL, 'radio', 3),
(1, 302, 300, 'Sabor', 'Dulce de leche', NULL, 'radio', 3),
(1, 303, 301, 'Extra', 'Bocha de helado', '60.00', 'checkbox', 3),
(1, 304, 301, 'Extra', 'Crema', '30.00', 'checkbox', 3),
(1, 305, 301, 'Extra', 'Dulce de leche', '30.00', 'checkbox', 3),
(1, 314, 302, 'Especies', 'Orégano', '15.00', 'checkbox', 4),
(1, 315, 302, 'Especies', 'Pimienta', '11.00', 'checkbox', 4),
(1, 316, 303, 'Especies', 'Orégano', '15.00', 'checkbox', 4),
(1, 317, 302, 'Especies', 'Pimienta', '11.00', 'checkbox', 4),
(1, 318, 304, 'Amor', 'Corazones', '55.00', 'checkbox', 5),
(1, 319, 305, 'Amor', 'Corazones', '55.00', 'checkbox', 5),
(2, 320, 1, 'Especias', 'Pimentón', NULL, 'checkbox', 6),
(2, 321, 2, 'Extras Pizza', 'Extra queso', '55.00', 'checkbox', 6),
(2, 322, 3, 'Picante', 'Picante', NULL, 'radio', 7),
(2, 323, 3, 'Picante', 'No picante', NULL, 'radio', 7),
(2, 324, 4, 'Picante', 'Picante', NULL, 'radio', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options_detail`
--

CREATE TABLE `options_detail` (
  `option_detail_id` int(7) NOT NULL,
  `order_detail_id` int(7) NOT NULL,
  `option_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `client_id` int(3) NOT NULL,
  `order_id` int(5) NOT NULL,
  `client` varchar(30) NOT NULL,
  `order_date` date NOT NULL,
  `order_time` time NOT NULL,
  `deliver_time` time NOT NULL,
  `order_price` decimal(6,2) NOT NULL,
  `order_notes` text,
  `dispatched` tinyint(1) NOT NULL DEFAULT '0',
  `paid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_detail`
--

CREATE TABLE `orders_detail` (
  `order_detail_id` int(7) NOT NULL,
  `order_id` int(5) NOT NULL,
  `product_id` int(3) NOT NULL,
  `quantity` int(2) NOT NULL,
  `order_detail_price` decimal(6,2) NOT NULL,
  `type_id` int(2) NOT NULL,
  `order_detail_notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders_timing`
--

CREATE TABLE `orders_timing` (
  `id` int(5) NOT NULL,
  `client_id` int(3) NOT NULL,
  `from_order` int(3) NOT NULL,
  `to_order` int(3) NOT NULL,
  `minutes` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `orders_timing`
--

INSERT INTO `orders_timing` (`id`, `client_id`, `from_order`, `to_order`, `minutes`) VALUES
(1, 1, 1, 10, 15),
(2, 1, 11, 20, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `client_id` int(3) NOT NULL,
  `product_id` int(3) NOT NULL,
  `product_name` varchar(15) NOT NULL,
  `desc_short` varchar(50) NOT NULL,
  `desc_long` text,
  `product_price` decimal(6,2) NOT NULL,
  `type_id` int(2) NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `available` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`client_id`, `product_id`, `product_name`, `desc_short`, `desc_long`, `product_price`, `type_id`, `img`, `available`) VALUES
(1, 1, 'Clásica', 'Hamburguesa con queso', 'La clásica hamburguesa de intenso sabor', '220.00', 0, NULL, 1),
(1, 2, 'Completa', 'Jamón, queso, lechuga, tomate, huevo', 'Poderosa hamburguesa con todo lo que tiene que tener', '300.00', 0, NULL, 1),
(1, 3, 'Americana', 'Panceta, chedar, cebolla', 'Con el sabor característicos de las hamburguesas americanas', '280.00', 0, NULL, 1),
(1, 4, 'Veggie', 'Hamburguesa de quinoa con queso vegano', 'De una increibla testura y sabor', '250.00', 0, NULL, 1),
(1, 5, 'Pollo', 'Hamburguesa de pollo, lechuga, huevo', 'Exquisita y liviana', '250.00', 0, NULL, 1),
(1, 6, 'Doble', 'Queso, panceta', 'Doble hamburguesa con queso y panceta', '340.00', 0, NULL, 1),
(1, 7, 'Triple', 'Queso, Panceta', 'La triple y poderosa!', '390.00', 0, NULL, 1),
(1, 8, 'papaleoleo', 'eg  erg e erg er', 'ggggg', '777.00', 0, NULL, 1),
(1, 100, 'Papas clásicas', 'Bien crocantes', NULL, '40.00', 1, NULL, 1),
(1, 101, 'Papas con cheda', 'Con abundante queso', NULL, '70.00', 1, NULL, 1),
(1, 102, 'Papas completas', 'Cheddar, panceta', NULL, '120.00', 1, NULL, 1),
(1, 200, 'Cerveza Tirada', 'Artesanal', NULL, '90.00', 2, NULL, 1),
(1, 201, 'Cerveza Vaso', 'Quilmes, Stella, Hessenbeck', NULL, '75.00', 2, NULL, 1),
(1, 202, 'Agua', 'Con gas, sin gas', NULL, '75.00', 2, NULL, 1),
(1, 203, 'Gaseosa', 'Línea Coca', NULL, '70.00', 2, NULL, 1),
(1, 300, 'Mousse', 'Postre de mousse de chocolate', NULL, '90.00', 3, NULL, 1),
(1, 301, 'Brownie', 'Bien esponjoso', NULL, '90.00', 3, NULL, 1),
(1, 311, 'Muzza', 'Muzza', 'Con Muzza', '159.00', 4, '', 1),
(2, 320, 'Muzza', 'Muzza, orégano', 'Con muuucha muza!', '250.00', 6, '', 1),
(2, 321, 'Burrito', 'Burro y cebolla', '\"Andaleee\"', '550.00', 7, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_options`
--

CREATE TABLE `products_options` (
  `client_id` int(3) NOT NULL,
  `product_option_id` int(5) NOT NULL,
  `product_id` int(3) NOT NULL,
  `option_id` int(3) NOT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `products_options`
--

INSERT INTO `products_options` (`client_id`, `product_option_id`, `product_id`, `option_id`, `selected`) VALUES
(1, 1, 1, 1, 1),
(1, 2, 1, 2, 0),
(1, 3, 1, 3, 1),
(1, 4, 1, 4, 0),
(1, 5, 1, 5, 1),
(1, 6, 1, 6, 0),
(1, 7, 1, 7, 0),
(1, 8, 1, 8, 0),
(1, 9, 1, 9, 0),
(1, 10, 1, 10, 0),
(1, 11, 1, 11, 0),
(1, 12, 1, 12, 0),
(1, 13, 2, 1, 1),
(1, 14, 2, 2, 0),
(1, 15, 2, 3, 1),
(1, 16, 2, 4, 0),
(1, 17, 2, 5, 1),
(1, 18, 2, 6, 0),
(1, 19, 2, 7, 0),
(1, 20, 2, 8, 0),
(1, 21, 2, 9, 0),
(1, 22, 2, 10, 0),
(1, 23, 2, 11, 0),
(1, 24, 2, 12, 0),
(1, 25, 3, 1, 1),
(1, 26, 3, 2, 0),
(1, 27, 3, 3, 1),
(1, 28, 3, 4, 0),
(1, 29, 3, 5, 1),
(1, 30, 3, 6, 0),
(1, 31, 3, 7, 0),
(1, 32, 3, 8, 0),
(1, 33, 3, 9, 0),
(1, 34, 3, 10, 0),
(1, 35, 3, 11, 0),
(1, 36, 3, 12, 0),
(1, 37, 4, 1, 1),
(1, 38, 4, 2, 0),
(1, 39, 4, 3, 0),
(1, 40, 4, 4, 1),
(1, 41, 4, 5, 1),
(1, 42, 4, 6, 0),
(1, 43, 4, 7, 0),
(1, 44, 4, 8, 0),
(1, 45, 4, 9, 0),
(1, 46, 4, 10, 0),
(1, 47, 4, 11, 0),
(1, 48, 4, 12, 0),
(1, 49, 5, 1, 0),
(1, 50, 5, 2, 1),
(1, 51, 5, 3, 1),
(1, 52, 5, 4, 0),
(1, 53, 5, 5, 1),
(1, 54, 5, 6, 0),
(1, 55, 5, 7, 0),
(1, 56, 5, 8, 0),
(1, 57, 5, 9, 0),
(1, 58, 5, 10, 0),
(1, 59, 5, 11, 0),
(1, 60, 5, 12, 0),
(1, 61, 6, 1, 1),
(1, 62, 6, 2, 0),
(1, 63, 6, 3, 1),
(1, 64, 6, 4, 0),
(1, 65, 6, 6, 1),
(1, 66, 6, 7, 0),
(1, 67, 6, 8, 0),
(1, 68, 6, 9, 0),
(1, 69, 6, 10, 0),
(1, 70, 6, 11, 0),
(1, 71, 6, 12, 0),
(1, 72, 7, 1, 1),
(1, 73, 7, 2, 0),
(1, 74, 7, 3, 1),
(1, 75, 7, 4, 0),
(1, 76, 7, 7, 1),
(1, 77, 7, 8, 0),
(1, 78, 7, 9, 0),
(1, 79, 7, 10, 0),
(1, 80, 7, 11, 0),
(1, 81, 7, 12, 0),
(1, 82, 100, 100, 1),
(1, 83, 100, 101, 0),
(1, 84, 101, 100, 1),
(1, 85, 101, 101, 0),
(1, 86, 102, 100, 1),
(1, 87, 102, 101, 0),
(1, 88, 200, 200, 1),
(1, 89, 200, 201, 0),
(1, 90, 200, 202, 1),
(1, 91, 200, 203, 0),
(1, 92, 200, 204, 0),
(1, 93, 201, 205, 1),
(1, 94, 201, 206, 0),
(1, 95, 201, 207, 0),
(1, 96, 202, 211, 0),
(1, 97, 202, 212, 1),
(1, 98, 203, 208, 1),
(1, 99, 203, 209, 0),
(1, 100, 203, 210, 0),
(1, 101, 300, 300, 0),
(1, 102, 300, 301, 1),
(1, 103, 300, 302, 0),
(1, 104, 301, 300, 0),
(1, 105, 301, 301, 1),
(1, 106, 301, 303, 0),
(1, 107, 301, 304, 0),
(1, 108, 301, 305, 0),
(1, 109, 311, 314, 0),
(1, 110, 311, 315, 0),
(2, 113, 320, 320, 0),
(2, 114, 320, 321, 0),
(2, 115, 321, 322, 0),
(2, 116, 321, 323, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products_tags`
--

CREATE TABLE `products_tags` (
  `product_tag_id` int(3) NOT NULL,
  `product_id` int(3) NOT NULL,
  `tag_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `role_id` int(1) NOT NULL,
  `role_name` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(0, 'common'),
(1, 'main_admin'),
(2, 'dispatcher'),
(3, 'cashier'),
(4, 'data_admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(2) NOT NULL,
  `tag_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags_options`
--

CREATE TABLE `tags_options` (
  `tag_option_id` int(3) NOT NULL,
  `tag_id` int(3) NOT NULL,
  `option_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types`
--

CREATE TABLE `types` (
  `client_id` int(3) NOT NULL,
  `type_id` int(2) NOT NULL,
  `display_order` int(2) NOT NULL,
  `type_name` varchar(15) NOT NULL,
  `type_icon` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `types`
--

INSERT INTO `types` (`client_id`, `type_id`, `display_order`, `type_name`, `type_icon`) VALUES
(1, 0, 1, 'plato', 'mdi-hamburger'),
(1, 1, 2, 'acompañamiento', 'mdi-popcorn'),
(1, 2, 3, 'bebida', 'mdi-glass-mug-variant'),
(1, 3, 4, 'postre', 'mdi-ice-pop'),
(1, 4, 0, 'pizza', 'mdi-pizza'),
(1, 5, 0, 'Enio', 'mdi-pot'),
(2, 6, 0, 'pizza', 'mdi-pizza'),
(2, 7, 0, 'Mexican', 'mdi-chili-mild');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `user_name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `person_name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `client_id` int(3) NOT NULL,
  `reset_password` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `password`, `person_name`, `client_id`, `reset_password`) VALUES
(1, 'fede.burger', '$2b$10$7n6tTjlnwhTqHjV1dY5Y0.GJVD86r5MCULtYMo3oJqxs6YW/ol1f6', 'Federico Hemberg', 1, 0),
(2, 'juan.burger', '$2b$10$YxW8Fu4GojwUjsdh/MUzROGSeiWtdZASdJVm58xqTFCcEsOJMrQoy', 'Juan Steinberg', 1, 0),
(3, 'fede.bronx', '$2b$10$7n6tTjlnwhTqHjV1dY5Y0.GJVD86r5MCULtYMo3oJqxs6YW/ol1f6', 'Fede Bronx', 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `users_roles_id` int(3) NOT NULL,
  `client_id` int(3) NOT NULL,
  `user_id` int(2) NOT NULL,
  `role_id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`users_roles_id`, `client_id`, `user_id`, `role_id`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2),
(3, 1, 1, 3),
(4, 1, 1, 4),
(5, 1, 2, 2),
(6, 1, 1, 0),
(7, 1, 2, 0),
(8, 2, 3, 0),
(9, 2, 3, 1),
(10, 2, 3, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`agent_id`),
  ADD KEY `client_id` (`client_id`);

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indices de la tabla `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`,`client_id`) USING BTREE,
  ADD KEY `type_id` (`type_id`);

--
-- Indices de la tabla `options_detail`
--
ALTER TABLE `options_detail`
  ADD PRIMARY KEY (`option_detail_id`) USING BTREE,
  ADD KEY `order_detail_id` (`order_detail_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`,`client_id`) USING BTREE;

--
-- Indices de la tabla `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`order_detail_id`) USING BTREE,
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indices de la tabla `orders_timing`
--
ALTER TABLE `orders_timing`
  ADD PRIMARY KEY (`id`,`client_id`) USING BTREE;

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`,`client_id`) USING BTREE,
  ADD KEY `type_id` (`type_id`);

--
-- Indices de la tabla `products_options`
--
ALTER TABLE `products_options`
  ADD PRIMARY KEY (`product_option_id`,`client_id`) USING BTREE,
  ADD KEY `product_id` (`product_id`),
  ADD KEY `option_id` (`option_id`);

--
-- Indices de la tabla `products_tags`
--
ALTER TABLE `products_tags`
  ADD PRIMARY KEY (`product_tag_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indices de la tabla `tags_options`
--
ALTER TABLE `tags_options`
  ADD PRIMARY KEY (`tag_option_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indices de la tabla `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`type_id`,`client_id`,`display_order`) USING BTREE;

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `Users` (`user_name`,`client_id`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`users_roles_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agents`
--
ALTER TABLE `agents`
  MODIFY `agent_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=325;

--
-- AUTO_INCREMENT de la tabla `options_detail`
--
ALTER TABLE `options_detail`
  MODIFY `option_detail_id` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `order_detail_id` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orders_timing`
--
ALTER TABLE `orders_timing`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=322;

--
-- AUTO_INCREMENT de la tabla `products_options`
--
ALTER TABLE `products_options`
  MODIFY `product_option_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT de la tabla `products_tags`
--
ALTER TABLE `products_tags`
  MODIFY `product_tag_id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(2) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tags_options`
--
ALTER TABLE `tags_options`
  MODIFY `tag_option_id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `types`
--
ALTER TABLE `types`
  MODIFY `type_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `agents`
--
ALTER TABLE `agents`
  ADD CONSTRAINT `agents_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`);

--
-- Filtros para la tabla `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`type_id`);

--
-- Filtros para la tabla `options_detail`
--
ALTER TABLE `options_detail`
  ADD CONSTRAINT `options_detail_ibfk_1` FOREIGN KEY (`order_detail_id`) REFERENCES `orders_detail` (`order_detail_id`),
  ADD CONSTRAINT `options_detail_ibfk_2` FOREIGN KEY (`option_id`) REFERENCES `options` (`option_id`);

--
-- Filtros para la tabla `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD CONSTRAINT `orders_detail_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`),
  ADD CONSTRAINT `orders_detail_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `orders_detail_ibfk_3` FOREIGN KEY (`type_id`) REFERENCES `types` (`type_id`);

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`type_id`);

--
-- Filtros para la tabla `products_options`
--
ALTER TABLE `products_options`
  ADD CONSTRAINT `products_options_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `products_options_ibfk_2` FOREIGN KEY (`option_id`) REFERENCES `options` (`option_id`);

--
-- Filtros para la tabla `products_tags`
--
ALTER TABLE `products_tags`
  ADD CONSTRAINT `products_tags_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`),
  ADD CONSTRAINT `products_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`);

--
-- Filtros para la tabla `tags_options`
--
ALTER TABLE `tags_options`
  ADD CONSTRAINT `tags_options_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
