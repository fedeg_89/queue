const quotesSpanish = [
'«El secreto para cambiar es concentrar toda tu energía no en luchar contra lo viejo sino en construir lo nuevo». – Sócrates, padre de la filosofía occidental.',
'«Entrega siempre más de lo que esperan de ti». – Larry Page, cofundador de Google.',
'«No tomes demasiados consejos. La mayoría de las personas que dan muchos consejos, con algunas excepciones, generalizan lo que hicieron. No analices demasiado. Yo mismo he sido culpable de pensar demasiado. Sólo construye cosas y descubre si funcionan”. – Ben Silbermann, fundador de Pinterest.',
'«Hay dos tipos de personas que te dirán que no puedes hacer una diferencia en este mundo: las que tienen miedo de intentarlo y las que tienen miedo de que tengas éxito». – Ray Goforth, director de estrategia en Spredfast.',
'«El éxito no es definitivo, el fracaso no es fatal: lo que realmente cuenta es el valor para continuar». – Winston Churchill, Primer Ministro de Gran Bretaña durante la Segunda Guerra Mundial.',
'«Si puedes superar esa sensación de estar asustado y de tomar riesgos, realmente pueden suceder cosas sorprendentes». – Marissa Mayer, presidenta y CEO de Yahoo!.',
'«Cada vez que lanzamos una nueva función, la gente nos grita». – Angelo Sotira, cofundador de deviantART.',
'«Sé innegablemente bueno. Ningún esfuerzo de marketing o de moda en las redes sociales puede sustituirlo». – Anthony Volodkin, fundador de Hype Machine.',
'«¿Qué necesitas para comenzar un negocio? Tres cosas simples: conocer tu producto mejor que nadie, conocer a tus clientes y tener un deseo ardiente de tener éxito». – Dave Thomas, fundador de Wendy’s.',
'«Si no puedes alimentar a un equipo con dos pizzas, es demasiado grande». – Jeff Bezos, fundador y CEO de Amazon.',
'«Si a la gente le gustas, te escucharán; pero si confían en ti, harán negocios contigo». – Zig Ziglar, autor, vendedor y orador motivacional.',
'«Si no estás avergonzado por la primera versión de tu producto, lo has lanzado demasiado tarde». – Reid Hoffman, cofundador de LinkedIn.',
'“El valor de una idea radica en su uso”. – Thomas Edison, cofundador de General Electric.',
'“Las estrellas nunca se alinearán, y los semáforos de la vida nunca serán todos verdes al mismo tiempo. El universo no conspira contra ti, pero tampoco se desvía para alinear tu camino. Las condiciones nunca son perfectas. La frase «algún día» es una enfermedad que llevará tus sueños a la tumba contigo. Las listas de pros y contras son igual de malas. Si es importante para ti y deseas hacerlo «eventualmente», simplemente hazlo y corrige el rumbo en el camino «. – Tim Ferriss, autor de La semana laboral de 4 horas.',
'«Haz que todos los detalles sean perfectos y limita la cantidad de detalles a la perfección». – Jack Dorsey, cofundador de Twitter.',
'“Tu trabajo va a llenar una gran parte de tu vida, y la única manera de estar realmente satisfecho es hacer lo que crees que es un gran trabajo. Y la única manera de hacer un gran trabajo es amar lo que haces «. – Steve Jobs, cofundador, Presidente y CEO de Apple Inc.',
'«Un hombre exitoso es aquél que puede hacer cimientos con los ladrillos que otros le han lanzado». – David Brinkley, presentador de noticias de NBC y ABC.',
'“Busca siempre al tonto en un trato. Si no encuentras uno, eres tú». – Mark Cuban, Presidente y empresario de AXS TV.',
'“Las personas exitosas hacen lo que las personas no exitosas no están dispuestas a hacer. No desees que sea más fácil; desea ser mejor». – Jim Rohn, uno de los filósofos de negocios más importantes de América.',
'«Tus clientes más infelices son tu mayor fuente de aprendizaje». – Bill Gates, cofundador de Microsoft.',
'«Si sólo trabajas en cosas que te gustan y te apasionan, no tienes que tener un plan maestro sobre cómo funcionarán las cosas». – Mark Zuckerberg, fundador de Facebook.',
'«Si te defines por cómo te diferencias de la competencia, seguramente estás en problemas». – Omar Hamoui, cofundador de AdMob.',
'«Si puedes soñarlo, puedes hacerlo». – Walt Disney, fundador de Disney Brother Studio y Disneyland.',
'“¿Quieres saber lo que el cliente realmente quiere? Pregúntale. No se lo digas.”- Lisa Stone, co-fundador y CEO de BlogHer.',
'“Si algo te apasiona y trabajas duro, entonces será un éxito.” – Pierre Omidyar, fundador y presidente de eBay.',
'“Si tienes una idea que no puedes dejar de pensar, probablemente sea una buena idea para llevar a cabo.” – Josh James, CEO y co-fundador de Omniture.',
'«No se trata de ideas. Se trata de hacer que las ideas se conviertan en realidad». – Scott Belsky, cofundador de Behance.',
'«Aprovecha al máximo tus propias chispas de posibilidades, y conviértelas en llamas de éxito». – Golda Meir, cuarta primera ministra de Israel.',
'«No hay nada de malo en mantenerse pequeño. Puedes hacer grandes cosas con un equipo pequeño». – Jason Fried, fundador de 37signals y coautor de Rework.',
'«La forma más rápida de cambiar es convivir con personas que ya son como quieres ser». – Reid Hoffman, cofundador de LinkedIn.',
'«Persigue la visión, no el dinero, el dinero terminará siguiéndote». – Tony Hsieh, CEO de Zappos.',
'“No hay secretos para el éxito. Es simplemente el resultado de la preparación, el trabajo duro y el aprendizaje del fracaso». – Colin Powell, estadista de EE. UU. y General de cuatro estrellas retirado en el Ejército de los Estados Unidos.',
'“El veneno más peligroso es el sentimiento de logro. El antídoto es pensar cada noche qué se puede hacer mejor mañana». – Ingvar Kamprad, fundador de IKEA.',
'«No te preocupes por el fracaso; sólo tienes que tener razón una vez.»- Drew Houston, fundador y CEO de Dropbox.',
'«Nada funciona mejor que simplemente mejorar tu producto». – Joel Spolsky, cofundador de Stack Overflow.',
'«Junta a cinco o seis de tus amigos más inteligentes en una habitación y pídeles que califiquen tu idea». – Mark Pincus, CEO de Zynga.',
'“El dinero es como la gasolina durante un viaje por carretera. No querrás quedarte sin combustible en tu viaje, pero tampoco estás haciendo un recorrido por todas las gasolineras». – Tim O’Reilly, fundador y CEO de O’Reilly Media.',
'“¿Quieres que te dé una fórmula para el éxito? Realmente es bastante simple: duplica tu tasa de fracasos. Estás pensando en el fracaso como el enemigo del éxito. Pero no lo es en absoluto. Puedes desanimarte por el fracaso o puedes aprender de él, así que sigue adelante y comete errores. Haz todo lo que puedas. Porque recuerda que ahí es donde encontrarás el éxito». – Thomas J. Watson, segundo presidente de IBM, figura política y filántropo.',
'“El diseño no es sólo lo que parece y lo que se siente. El diseño es cómo funciona». – Steve Jobs, cofundador, presidente y CEO de Apple Inc.',
'«El camino hacia el éxito y el camino hacia el fracaso son casi exactamente los mismos». – Colin R. Davis, director de orquesta de la London Symphony Orchestra.',
'«Hazte grande en silencio, para no avisar a los posibles competidores». – Chris Dixon, un inversionista de Andreessen Horowitz.',
'«Algunas personas sueñan con tener éxito, mientras que otras se levantan cada mañana y lo hacen realidad». – Wayne Huizenga, prolífico empresario estadounidense, propietario de Blockbuster Video y los Miami Dolphins.',
'“Las personas que triunfan tienen impulso. Mientras más éxito tienen, más quieren tener éxito y más encuentran la manera de tener éxito. De manera similar, cuando alguien está fallando, la tendencia es ir en una espiral descendente que incluso puede convertirse en una profecía autocumplida». – Tony Robbins, estratega de vida y negocios, y autor.',
'“El carácter no puede desarrollarse con facilidad y tranquilidad. Sólo a través de la experiencia y el sufrimiento se puede fortalecer al alma, la ambición se inspira y se logra el éxito». – Helen Keller, autora estadounidense, activista política y conferencista.',
'«No trates de ser original, sólo trata de ser bueno». – Paul Rand, Diseñador gráfico.',
'«Siempre piensa afuera de la caja y aprovecha las oportunidades que aparezcan, dondequiera que estén». – Lakshmi Mittal, Presidente y CEO de ArcelorMittal.',
'«No te preocupes por las personas que roban tus diseños. Preocúpate el día en que dejen de hacerlo». – Jeffrey Zeldman, empresario y diseñador web.',

48. «Por más difícil que parezca la vida, siempre hay algo que puedes hacer y en lo que puedes tener éxito». – Stephen Hawking, físico teórico inglés, cosmólogo y autor.

49. «El último 10% que se necesita para lanzar algo requiere tanta energía como el primer 90%». – Rob Kalin, fundador de Etsy.

50. «La prueba real no es si puedes evitar fallar, porque no podrás. La prueba es si vas a dejar que te limite o te inmovilice, o si vas a aprender de ello y eliges perseverar”. – Barack Obama, 44º Presidente de los Estados Unidos.

¡Bien hecho, ya has llegado a la mitad de nuestra lista de las 100 mejores frases de motivación para negocios!

51. «Si te interesa verdaderamente lo que haces, concéntrate en construir cosas en lugar de hablar de ellas». – Ryan Freitas, cofundador de About.me.

52. «Nuestra mayor debilidad radica en rendirse. La forma más segura de triunfar siempre es intentarlo sólo una vez más «. – Thomas Edison, empresario e inventor.

53. «Las mejores startups generalmente provienen de alguien que necesita resolver un problema». – Michael Arrington, fundador y coeditor de TechCrunch.

54. «Cae siete veces, levántate ocho». – Proverbio japonés.

Vamos a dejarte un par de frases inspiradoras más de Tony Hsieh aquí.

55. «No juegues juegos que no entiendas, incluso si ves que muchas otras personas ganan dinero con ellos». – Tony Hsieh, CEO de Zappos.

56. «El tiempo, la perseverancia y diez años de intentarlo eventualmente te harán ver como un éxito de la noche a la mañana». – Biz Stone, co-fundador de Twitter.



57. “Un emprendedor es alguien que tiene una visión de algo y quiere crear”. – David Karp, fundador y CEO de Tumblr.

58. “Tener ideas es fácil. Implementarlas es difícil. ”- Guy Kawasaki, empresario y cofundador de Alltop.

59. “Tuve suerte porque nunca abandoné la búsqueda. ¿Estás abandonando el barco demasiado pronto? O, ¿estás dispuesto a perseguir tu suerte con ahínco? ”- Jill Konrath, oradora, autora y líder de opinión.

60. «Consigue un mentor en el campo correspondiente si no estás seguro de lo que estás buscando». – Kyle Bragger, fundador de Forrst y cofundador de Exposure.

61. “Ganas fuerza, valor y confianza con cada experiencia en la que realmente miras al miedo a la cara. Eres capaz de decirte a ti mismo, viví este horror. Puedo hacer lo siguiente que venga. Debes hacer lo que crees que no puedes hacer». – Eleanor Roosevelt, conocida como «Primera Dama del Mundo» por su defensa de los derechos humanos.

62. «Cada día que pasamos sin mejorar nuestros productos es un día perdido». – Joel Spolsky, cofundador de Stack Overflow.

63. «Lo único peor que comenzar algo y fallar… es no comenzarlo en absoluto». – Seth Godin, autor, empresario y bloguero.

Las frases motivacionales de negocios como ésta seguramente te empujarán a tomar acción.

64. «El éxito consiste en ir de un fracaso a otro sin perder el entusiasmo». – Winston Churchill, Primer Ministro de Gran Bretaña durante la Segunda Guerra Mundial.

65. «Todo lo que se puede medir y observar, mejora». – Bob Parsons, fundador de GoDaddy.

66. «Mantente autofinanciado el mayor tiempo posible». – Garrett Camp, fundador de Expa, Uber y StumbleUpon.

67. «Cuando esté viejo y moribundo, planeo mirar hacia atrás en mi vida y decir ‘wow, eso fue una aventura’, no ‘wow, me sentí seguro'». – Tom Preston-Werner, cofundador de Github.

68. “Los seres humanos tienen un impulso interno innato para ser autónomos, autodeterminados y conectados entre sí. Y, cuando ese impulso se libera, la gente logra más y vive vidas más plenas». – Daniel Pink, autor.

69. “Las fortunas se construyen en el mercado popular y se recolectan en el mercado de lujo”. – Jason Calacanis, fundador de LAUNCH Ticker.

La siguiente es una de mis frases de motivación de negocios favoritas.

70. «Nunca soñé con el éxito, trabajé para ello». – Estee Lauder, fundadora de Estee Lauder Cosmetics.

 



71. «Si no puedes volar, entonces corre. Si no puedes correr, entonces camina. Y, si no puedes caminar, entonces gatea, pero hagas lo que hagas, debes seguir avanzando». – Martin Luther King Jr., líder del movimiento de los derechos civiles.

72. «Es más efectivo hacer algo valioso que esperar que un logotipo o nombre lo haga por ti». – Jason Cohen, fundador de Smartbear Software.

73. «El éxito generalmente llega a aquellos que están demasiado ocupados para buscarlo». – Henry David Thoreau, ensayista, poeta y filósofo.

74. “No hay que romantizar sobre lo genial que es ser un emprendedor. Es una lucha para salvar la vida de tu empresa, y tu propia piel, todos los días de la semana». – Spencer Fry, cofundador de CarbonMade.

75. «Trato de no tomar ninguna decisión que no me entusiasme». – Jake Nickell, fundador y CEO de Threadless.

Bien hecho – ya llevamos 75 frases motivacionales de negocios, faltan 25 más para el final…

76. «Ve las cosas como si estuvieran en el presente, incluso si están en el futuro». – Larry Ellison, cofundador de Oracle.

77. «No puedes hacer que algo se haga viral, pero puedes hacer algo bueno». – Peter Shankman, fundador de HARO.

78. «El éxito es la suma de pequeños esfuerzos, que se repiten día tras día». – Robert Collier, autor.

79. «No te preocupes por la financiación si no la necesitas. Hoy en día, es más barato comenzar un negocio que nunca». – Noah Everett, fundador de Twitpic.

80. «Los datos superan a las emociones». – Sean Rad, fundador de Tinder.

81. «No es necesario tener una compañía de 100 personas para desarrollar una idea». – Larry Page, cofundador de Google.

Estas frases de emprendedores también son excelentes para pequeñas empresas.

82. “Las ideas son un producto. La ejecución de ellas no lo es”. – Michael Dell, presidente y CEO de Dell.

83. «Sabía que si fallaba no me arrepentiría, pero sabía que me lamentaría por no intentarlo». – Jeff Bezos, fundador y director ejecutivo de Amazon.

84. «Ya sea que pienses que puedes o que no puedes, tienes razón». – Henry Ford, fundador de Ford Motor Company.



85. «Todos los seres humanos son emprendedores, no porque deban crear empresas, sino porque la voluntad de crear está codificada en el ADN humano». – Reid Hoffman, cofundador de LinkedIn.

86. “La primera regla de cualquier tecnología utilizada en una empresa es que la automatización aplicada a una operación eficiente aumentará la eficiencia. La segunda regla es que la automatización aplicada a una operación ineficiente aumentará la ineficiencia». – Bill Gates, cofundador de Microsoft.

87. «Al final, una visión sin la capacidad de ejecutarla es probablemente una alucinación». – Steve Case, cofundador de AOL.

88. “Sal de la historia que te está frenando. Entra en la nueva historia que estás dispuesto a crear». – Oprah Winfrey, propietaria de medios de comunicación.

89. «No importa lo brillante que sea tu mente o tu estrategia, si juegas un juego en solitario, siempre perderás ante un equipo». – Reid Hoffman, cofundador de LinkedIn.

90. «No seas engreído. No seas arrogante. Siempre hay alguien mejor que tú». – Tony Hsieh, CEO de Zappos.

Estas frases motivacionales pueden ser contundentes, pero son bastante precisas.

91. «Acepta lo que no sabes, especialmente al principio, porque lo que no sabes puede convertirse en tu mayor activo. Te asegura que harás cosas diferentes a todos los demás». – Sara Blakely, fundadora de SPANX.

92. «Me parece que cuanto más trabajo, más suerte tengo». – Thomas Jefferson, fundador y presidente de los Estados Unidos.

93. “La intrepidez es como un músculo. Sé por propia experiencia que cuanto más lo hago, más natural se vuelve no dejar que mis temores me dominen». – Arianna Huffington, presidenta de The Huffington Post Media Group.

94. «La diligencia es la madre de la buena suerte». – Benjamin Franklin, un padre fundador de los Estados Unidos.

95. «No importa cuántas veces falles. No importa cuántas veces casi lo haces bien. Nadie va a saber o preocuparse por tus fracasos, y tú tampoco deberías. Todo lo que tienes que hacer es aprender de ellos y de quienes te rodean, porque lo único que importa en los negocios es que lo hagas bien una vez. Entonces todos pueden decirte lo afortunado eres». – Mark Cuban, empresario, propietario de Landmark Theatres y presidente de AXS TV.

96. “Arriesga más de lo que otros piensan que es seguro. Sueña más de lo que otros piensan que es práctico». – Howard Schultz, CEO de Starbucks.



Estas frases de motivación para negocios son increíbles.

97. «Una persona que es tranquilamente confiada es el mejor líder». – Fred Wilson, cofundador de Union Square Ventures.

98. «La forma de comenzar es dejar de hablar y comenzar a hacer». – Walt Disney, fundador de Disney Brother Studio y Disneyland.

99. «A menudo la gente trabaja duro en algo que no es para ellos. Trabajar en lo que está bien para ti es probablemente más importante que trabajar duro». – Caterina Fake, cofundadora de Flickr.

Y por último, pero no menos importante, en nuestra lista de frases motivacionales de negocios…

100. “Nunca me tomé un día libre durante mis veintes. Ni uno solo». – Bill Gates, cofundador de Microsoft.

¡Bien hecho, ya has llegado a la mitad de nuestra lista de las 200 mejores frases de motivación para negocios!

101. «Debo mi éxito a haber escuchado respetuosamente los mejores consejos, y luego hacer exactamente lo contrario». – G. K. Chesterton, escritor, poeta y filósofo inglés.

102. «Si te fijas bien, la mayoría de los éxitos que pasaron de la noche a la mañana tomaron mucho tiempo». – Steve Jobs, cofundador, presidente y CEO de Apple Inc.

103. “El éxito parece estar conectado con la acción. Las personas exitosas siempre están moviéndose. Cometen errores, pero nunca renuncian». – Conrad Hilton, fundador de la cadena de hoteles Hilton.

Frases motivacionales sobre no rendirse
 Nunca te rindas. Sigue persiguiendo tus sueños. La vida será difícil a veces, pero la fuerza radica en no rendirse. Echa un vistazo a las mejores frases de emprendedores acerca de no rendirse.

104. «Si no puedes volar, entonces corre. Si no puedes correr, entonces camina. Y si no puedes caminar, entonces gatea, pero hagas lo que hagas, tienes que seguir avanzando». – Martin Luther King Jr., líder del movimiento por los derechos civiles.

105. “Tuve suerte porque nunca abandoné la búsqueda. ¿Estás pensando en abandonarlo todo demasiado pronto? O, ¿estás dispuesto a perseguir tus sueños como si fuera venganza? ”- Jill Konrath, oradora, autora y líder de opinión.

106. «Cae siete veces, levántate ocho». – Proverbio japonés

107. «La prueba real no consiste en evitar fallar, porque lo harás. La prueba real consiste en si te dejas endurecer, o si aprendes de ello; si eliges perseverar”. – Barack Obama, 44º presidente de los Estados Unidos.

108. «Nuestra mayor debilidad radica en renunciar. La forma más segura de triunfar siempre es intentarlo sólo una vez más «. – Thomas Edison, empresario e inventor.

109. “El pesimista ve dificultad en cada oportunidad. El optimista ve la oportunidad en cada dificultad «. – Winston Churchill, Primer Ministro de Gran Bretaña durante la Segunda Guerra Mundial.

Las frases de emprendedores como éstas te dan un impulso positivo.

110. «Tienes que creer en ti mismo cuando nadie más lo hace – eso es lo que te convierte en un ganador». – Venus Williams, tenista profesional estadounidense.

111. «Podemos tener muchas derrotas, pero no debemos ser derrotados». Maya Angelou, poeta, cantante y activista estadounidense de los derechos civiles.

112. «La fortaleza se muestra no sólo en la capacidad de persistir, sino en la capacidad de volver a empezar». – F. Scott Fitzgerald, escritor de ficción estadounidense.

113. “Déjame contarte el secreto que me ha llevado al éxito. Mi fuerza reside únicamente en mi tenacidad «. – Louis Pasteur, biólogo francés, microbiólogo y químico.

114. “Nunca renuncies a un sueño sólo por el tiempo que tomará lograrlo. El tiempo pasará de todos modos «. – Earl Nightingale, locutor de radio estadounidense y autor.

115. «Haz lo que puedas por el tiempo que puedas, y cuando ya no puedas más, haz la siguiente mejor cosa. Puedes retroceder, pero nunca darte por vencido». – Chuck Yeager, ex oficial de la Fuerza Aérea de los Estados Unidos.

116. «Lo intentaste. Fallaste. No importa. Inténtalo de nuevo. Falla nuevamente. Falla mejor». – Samuel Beckett, novelista y dramaturgo irlandés.

117. «Está bien celebrar el éxito, pero es más importante prestar atención a las lecciones del fracaso». – Bill Gates, cofundador de Microsoft.

Frases motivacionales sobre el trabajo duro
El trabajo duro ayuda a convertir los sueños en realidad. Las siguientes frases motivacionales sobre el trabajo duro te ayudarán a finalizar el trabajo.

118. «Muy a menudo la gente está trabajando duro en la cosa incorrecta. Trabajar en lo correcto es probablemente más importante que trabajar duro». – Caterina Fake, cofundadora de Flickr.

119. «Si te apasiona algo y trabajas duro, creo que tendrás éxito». – Pierre Omidyar, fundador y presidente de Ebay.

120. «Me parece que cuanto más trabajo, más suerte tengo». – Thomas Jefferson, fundador y presidente de los Estados Unidos.

121. «Cuando nos esforzamos por ser mejores que nosotros mismos, todo a nuestro alrededor también mejora». – Paulo Coelho, letrista y novelista brasileño.

122. “Un sueño no se hace realidad a través de la magia; requiere sudor, determinación y trabajo duro». – Colin Powell, estadista de los EE. UU. y general de cuatro estrellas retirado del Ejército de los Estados Unidos.

123. «Simplemente no hay sustituto para el trabajo duro cuando se trata de lograr el éxito». – Heather Bresch, CEO de Mylan.

Esperamos que estas frases inspiracionales te motiven a trabajar arduamente para lograr tu objetivo.

124. “El éxito no es un accidente. Es trabajo duro, perseverancia, aprendizaje, estudio, sacrificio y, sobre todo, amor por lo que estás haciendo o aprendiendo a hacer». – Pelé, futbolista profesional brasileño jubilado.

125. «Ningún gran logro, incluso aquéllos que lo hicieron parecer fácil, alguna vez tuvieron éxito sin trabajar duro». – Jonathan Sacks, autor y político británico.

126. «Lo único que supera la mala suerte es el trabajo duro». – Harry Golden, escritor estadounidense.

127. «No hay sustituto para el trabajo duro». – Thomas Edison, empresario e inventor.

128. «Es difícil vencer a una persona que nunca se rinde». – Babe Ruth, jugador estadounidense profesional de béisbol.

Frases motivacionales sobre desafíos
Todos enfrentamos retos en la vida. Pueden hacerte crecer o romperte. Hemos compilado una lista de nuestras frases motivacionales favoritas sobre desafíos que te alentarán a continuar.

129. «No tengas miedo de renunciar a algo bueno para ir por algo grandioso». – John D. Rockefeller, magnate empresarial estadounidense de la industria petrolera.

130. «Si no estás dispuesto a arriesgar tendrás que conformarte con lo ordinario». – Jim Rohn, empresario estadounidense, autor y orador motivacional.

131. “La intrepidez es como un músculo. Sé por experiencia propia que cuanto más lo hago, más natural se vuelve no dejar que mis temores me dominen». – Arianna Huffington, presidenta de The Huffington Post Media Group.

132. “Ganas fuerza, valor y confianza con cada experiencia en la que realmente te paras a mirar el miedo a la cara. Eres capaz de decirte a ti mismo, viví este horror. Puedo tomar lo siguiente que viene. Debes hacer lo que crees que no puedes hacer». – Eleanor Roosevelt, conocida como la «Primera Dama del Mundo «por su defensa por los derechos humanos.

133. «Si puedes superar esa sensación de estar asustado, esa sensación de riesgo, pueden suceder cosas realmente sorprendentes». – Marissa Mayer, presidenta y directora general de Yahoo!

134. «Si crees que puedes ya estás a mitad del camino». – Theodore Roosevelt, 26° presidente de los Estados Unidos.

135. «Los desafíos son lo que hace que la vida sea interesante, y superarlos es lo que hace que la vida tenga sentido». – Joshua J. Marine, autor.

136. «Los obstáculos no tienen que detenerte. Si te topas con una pared, no te des la vuelta y te rindas. Descubre cómo escalarla, atravesarla o darle la vuelta». – Michael Jordan, ex jugador de baloncesto profesional.

Estas frases motivacionales sobre desafíos tienen algunos consejos bastante sólidos.

137. «Siempre parece imposible hasta que alguien lo hace». – Nelson Mandela, ex presidente de Sudáfrica, líder político y filántropo.

138. “No soy producto de mis circunstancias. Soy producto de mis decisiones «. – Stephen Covey, educador estadounidense, autor y hombre de negocios.

139. «No se mide a un hombre sabiendo dónde se encuentra en momentos de comodidad y conveniencia, sino dónde se encuentra en momentos de desafío y controversia». – Martin Luther King, Jr., líder del movimiento de derechos civiles.

140. «Ser desafiado en la vida es inevitable, ser derrotado es opcional». – Roger Crawford, orador público, autor y profesional de tenis certificado.

141. “Los desafíos de la vida no deben paralizarte, deben ayudarte a descubrir quién eres”. – Bernice Johnson Reagon, compositora y activista social.

142. “Cuando menos lo esperamos, la vida nos presenta un desafío para poner a prueba nuestro valor y nuestra voluntad de cambiar; en ese momento, no tiene sentido fingir que no ha ocurrido nada o decir que aún no estamos listos. El desafío no esperará. La vida no mira hacia atrás «. – Paulo Coelho, letrista y novelista brasileño.

143. «Hay dos tipos de personas que te dirán que no puedes hacer una diferencia en este mundo: aquéllos que temen intentarlo y aquéllos que temen que tengas éxito». – Ray Goforth, director de estrategia y asociaciones en Spredfast.

Frases motivacionales sobre superar el fracaso
Habrá días en los que las cosas no salgan según lo planeado. Puedes sentirte derrotado y decepcionado. Pero eso es parte de la vida. Se trata de cómo lidiar con los fracasos en la vida lo que te hace ser quien eres. Aquí hay una lista de nuestras frases de emprendedores favoritas sobre cómo superar el fracaso que te motivarán a mantenerte fuerte.

144. «Es mejor fallar siendo original que tener éxito imitando a alguien más». – Herman Melville, novelista estadounidense.

145. «No te avergüences de tus fracasos, aprende de ellos y comienza de nuevo». – Richard Branson, fundador de Virgin Group.

146. «No importa cuántas veces falles. No importa cuántas veces casi lo haces bien. Nadie va a saber o preocuparse por tus fracasos, y tú tampoco deberías. Todo lo que tienes que hacer es aprender de ellos y de quienes te rodean, porque lo único que importa en los negocios es que lo hagas bien una vez. Entonces todos pueden decirte lo afortunado que eres». – Mark Cuban, empresario, propietario de Landmark Theatres y presidente de AXS TV.

147. «Sabía que si fallaba no me arrepentiría de eso, pero sabía que lo único que lamentaría sería no haberlo intentado». – Jeff Bezos, fundador y CEO de Amazon.

148. «Lo único peor que comenzar algo y fallar…es no comenzar». – Seth Godin, autor, empresario y blogger.

149. “No te preocupes por el fracaso; sólo tienes que lograrlo una vez.»- Drew Houston, fundador y CEO de Dropbox.

150. «El fracaso nunca me alcanzará si mi determinación de tener éxito es lo suficientemente fuerte». – Og Mandino, autor estadounidense.

151. «Todo lo que siempre has querido está del otro lado del miedo». – George Addair, desarrollador de bienes raíces.

152. «No te preocupes por fallar, preocúpate por las posibilidades que pierdes cuando ni siquiera lo intentas». – Jack Canfield, autor estadounidense, orador motivacional, capacitador corporativo y empresario.

Frases de emprendedores como éstas sobre la superación del miedo realmente te dan un impulso de inspiración.

153. «Muchos de los fracasos en la vida les sucedieron a aquéllos que no se dieron cuenta de lo cerca que estaban del éxito cuando se dieron por vencidos». – Thomas Edison, empresario e inventor.

154. «El único error real es aquél del cual no aprendes nada». – Henry Ford, fundador de Ford Motor Company.

155. «El fracaso se convierte en éxito si aprendes de él». – Malcolm Forbes, empresario estadounidense y editor de la revista Forbes.

156. «Fracasar es muy importante. Hablamos del éxito todo el tiempo. Es la capacidad de resistir fallar o a utilizar los errores lo que a menudo conduce a un mayor éxito. He conocido a personas que no quieren intentarlo por temor a fracasar». – J.K. Rowling, novelista británica.

157. «He fallado más de 9000 tiros en mi carrera. He perdido casi 300 juegos. Veintiséis veces me han confiado en tomar el juego, tirando y fallando. He fallado una y otra vez en mi vida. Y es por eso por lo que tengo éxito». – Michael Jordan, ex jugador de baloncesto profesional.

Frases motivacionales divertidas para animarte

Todos tenemos un mal día de vez en cuando. Estas frases motivacionales seguramente te inspirarán y te harán sonreír.

158. «La gente a menudo dice que la motivación no dura. Bueno, tampoco bañarse, por eso lo recomendamos a diario». – Zig Ziglar, autor estadounidense.

159. «Siempre quise ser alguien, pero ahora me doy cuenta de que debería haber sido más específica». – Lily Tomlin, actriz estadounidense.

160. «El camino hacia el éxito está salpicado de muchos espacios tentadores de estacionamiento «. – Will Rogers, actor estadounidense.

161. «No fallé la prueba. Más bien encontré 100 formas de hacerlo mal». – Benjamin Franklin, fundador de los Estados Unidos.

162. «Cuando escucho a alguien suspirar y decir que la vida es dura, siempre me siento tentado a preguntar, ‘¿comparada con qué?'». – Sydney Harris, periodista estadounidense.

163. «El ascensor al éxito está fuera de servicio. Tendrás que usar las escaleras …un paso a la vez. «- Joe Girard, vendedor estadounidense.

164. «La mayoría de la gente pierde la oportunidad porque la oportunidad está vestida de uniforme y parece trabajo». – Thomas Edison, empresario e inventor.

165. “Debes aprender de los errores de los demás. Posiblemente no puedas vivir el tiempo suficiente como para hacerlos todos tú mismo». – Sam Levenson, humorista estadounidense.

166. «Escucha, sonríe, asiente, y luego haz lo que sea que vayas a hacer de todos modos». – Robert Downey Jr., actor estadounidense.

167. «Recuerda, hoy es el mañana que te preocupó ayer». – Dale Carnegie, escritora estadounidense.

168. «Incluso si estás en el camino correcto, serás atropellado si te sientas allí». – Will Rogers, actor estadounidense.

Frases motivacionales de películas
Las películas tienen un cierto aspecto memorable en ellas. Tienen una forma única de enseñarnos lecciones que permanecen en nuestra memoria. Ya sea haciéndonos sonreír, o haciéndonos llorar, las películas tienen magia. Éstas son algunas de las mejores frases motivacionales de películas.

169. «Nadie va a golpearte tan fuerte como la vida. Lo difícil es ser golpeado y seguir avanzando. Es qué tantos golpes puedes tomar, y aún así seguir adelante. Así es como se gana”. – Rocky, Rocky Balboa.

170. «Nunca dejes que alguien te diga que no puedes hacer algo, ni siquiera yo. ¿Me escuchas? Si tienes un sueño, tienes que protegerlo. Las personas que no pueden hacer algo por sí mismas van a decirte que no puedes hacerlo. Si quieres algo, ve y obtenlo. Punto». – Chris Gardner, La búsqueda de la Felicidad.

171. «Lo único que se interpone entre tú y tu objetivo es la historia de mierda que sigues contándote a ti mismo sobre por qué no puedes lograrlo». – Jordan Belfort, El Lobo de Wall Street

172. «Todo lo que tenemos que decidir es qué hacer con el tiempo que se nos da». – Gandalf, el Señor de los Anillos: La comunidad del Anillo.

173. «Los grandes hombres no nacen grandes, se hacen grandes». – Vito Corleone, El Padrino.

174. «Algunas personas no pueden creer en sí mismas hasta que alguien más cree en ellas». – Sean Maguire, Good Will Hunting.

175. «¿Por qué caemos señor? Para que podamos aprender a levantarnos». – Alfred, Batman Comienza.

176. “La vida se mueve bastante rápido. Si no te detienes y miras a tu alrededor de vez en cuando, podrías perdértela». – Ferris, El día libre de Ferris Bueller.

177. «Después de un tiempo aprendes a ignorar los nombres con los que te llaman y a confiar en quién eres». – Shrek, Shrek The Third.

178. «Nuestras vidas están definidas por oportunidades, incluso las que perdemos». – Benjamin Button, El curioso caso de Benjamin Button.

179. «Lo que hacemos en la vida hace eco en la eternidad». – Maximus, Gladiador.

180. «A veces son las personas de las que nadie imagina nada quienes hacen las cosas que nadie puede imaginar». – Alan Turing, The Imitation Game.

181. «Todos los hombres mueren, pero no todos los hombres realmente viven». – William Wallace, Braveheart.

182. «Alguien que conocí una vez escribió que nos alejamos de nuestros sueños temiendo que podamos fallar, o peor, temiendo que tengamos éxito». – Forrester, Buscando a Forrester.

183. “No son nuestras habilidades las que muestran lo que realmente somos. Son nuestras decisiones.»- Dumbledore, Harry Potter y la Cámara de los Secretos.

184. «No debe haber límites para el esfuerzo humano. Todos somos diferentes. Por muy mal que parezca la vida, siempre hay algo que puedes hacer y en lo que puedes tener éxito. Mientras haya vida, hay esperanza «. – Stephen Hawking, La Teoría del Todo.

185. “Los accidentes son tu entrenamiento. La vida es una elección. Puedes elegir ser una víctima o cualquier otra cosa que quieras ser «. – Sócrates, de Peaceful Warrior.

186. «En esta vida no tienes que demostrarle nada a nadie, excepto a ti mismo. Y después de lo que has pasado, si no lo has hecho a estas alturas, no va a suceder nunca «. – Fortune, Rudy.

187. “Se supone que sea difícil. Si no fuera difícil, todos lo harían. Lo difícil es lo que lo hace genial”. – Jimmy Dugan, A League of Their Own.

188. «Oh sí, el pasado puede doler. Pero puedes huir de él o aprender de él». – Rafiki, El Rey León.

189. «No importa lo que alguien te diga, las palabras y las ideas pueden cambiar el mundo». – John Keating, La Sociedad de los Poetas Muertos.

190. «No debes tener miedo de soñar un poco más grande, cariño». – Eames, Inception.

191. “Encuentra una idea verdaderamente original. Es la única forma en la que me distinguiré. Es la única manera en la que siempre tendré importancia». – John Nash, Una Mente Brillante.

Frases motivacionales sobre el trabajo
No importa qué día de la semana sea, esta lista de frases motivacionales sobre el trabajo seguramente te ayudará a superar el día.

192. «Si estás trabajando en algo que realmente te importa, no tienes que ser empujado. La visión es lo que te atrae». – Steve Jobs, cofundador, presidente y director ejecutivo de Apple Inc.

193. «No digas que no tienes suficiente tiempo. Tienes exactamente la misma cantidad de horas por día que tuvieron Helen Keller, Pasteur, Miguel Ángel, la Madre Teresa, Leonardo Da Vinci, Thomas Jefferson y Albert Einstein”. —H. Jackson Brown Jr., autor estadounidense.

194. «Cuanto más quiero hacer algo, menos lo llamo trabajo». – Richard Bach, escritor estadounidense.

195. «No cuentes los días, haz que los días cuenten». – Muhammad Ali, boxeador profesional estadounidense.

196. «Dentro de un año, es posible que desees haber comenzado hoy». – Karen Lamb, autora.

197. «Algún día no es un día de la semana». – Janet Dailey, autora.

198. «Lo que más tememos es hacer lo que más necesitamos hacer». – Ralph Waldo Emerson, ensayista, filósofo y poeta.

199. «Haz de cada día tu obra maestra». – John Wooden, jugador de baloncesto estadounidense.

200. «El placer en el trabajo pone la perfección en el trabajo». – Aristóteles, padre de la filosofía occidental.