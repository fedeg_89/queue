import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'
import VueSocketIO from 'vue-socket.io'
// import LoginMixin from './mixins/loginMixin'
import store from './store'

import VuetifyToast from 'vuetify-toast-snackbar'
 
Vue.use(VuetifyToast)

Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:3000/adminNamespace',
}))

Vue.use(VueRouter)

Vue.config.productionTip = false

import AdminDashboard from './adminModule/adminDashboard'
import AdminProducts from './productsModule/adminProducts'
import NewProduct from './productsModule/newProduct/newProduct'
import AdminTypes from './typesModule/adminTypes'
import AdminOptions from './optionsModule/adminOptions'
import ProductOptionsForm from './productsOptionsModule/productOptionsForm'
import AdminOrders from './adminOrdersModule/adminOrders'
import AdminUsers from './usersModule/adminUsers'
import AdminLogin from './adminModule/adminLogin'
import AdminResetPassword from './adminModule/adminResetPassword'
import ConfigComponent from './configModule/configComponent'

const routes = [
  {path: '/', redirect: '/dashboard'},
  {path: '/dashboard', component: AdminDashboard, meta: {role: 0} },
  {path: '/products', component: AdminProducts, meta: {role: 4} },
  {path: '/newProduct', component: NewProduct, meta: {role: 4} },
  {path: '/types', component: AdminTypes, meta: {role: 4} },
  {path: '/options', component: AdminOptions, meta: {role: 4} },
  {path: '/productOptions', name:'productOptionsForm', component: ProductOptionsForm, props: true, meta: {role: 4} },
  {path: '/orders', component: AdminOrders, meta: {role: 2} },
  {path: '/users', component: AdminUsers, meta: {role: 1} },
  {path: '/config', component: ConfigComponent, meta: {role: 1} },
  {path: '/login', component: AdminLogin, meta: {role: 0} },
  {path: '/resetPassword', name: 'resetPassword', component: AdminResetPassword, meta: {role: 0}, props: true },

  {path: '*', redirect: '/dashboard'},
]


const router = new VueRouter({
  routes
})

router.beforeEach(async (to, from, next) => {
  const url= 'http://localhost:3000/api/auth'
  const authData = await fetch(url, {headers: new Headers([ ['authorization', localStorage.getItem('authorization')] ])})
  const auth = await authData.json()
  console.log('auth: ', auth)
  // Acces Token checking:
  switch (auth.msg) {
    case 'ok':
      console.log('ok')
      // If reset password
      if (auth.authData.reset_password) {
        store.commit('auth/RESET_PASSWORD', true)
        to.path == '/resetPassword' ? next() : next({ name: 'resetPassword', params: {user_id: auth.authData.user_id} }) // next('/resetPassword')
      } else {
        store.commit('auth/SET_ROLES', auth.authData.roles)
        if (to.matched.some(route => auth.authData.roles.some(role => role == route.meta.role))) {
          console.log('match')
          to.path != '/login' ? next() : next('/dashboard')
        } else {
          console.log('no rol')
          next('/dashboard')
        }
      }
      break

    case 'old':
      console.log('old: ', auth.err)
      localStorage.removeItem('authorization')
      store.commit('auth/AUTH_LOGOUT')
      to.path != '/login' ? next('/login') : next()
      break

    case 'no':
      console.log('no token')
      to.path != '/login' ? next('/login') : next()
      break
  }
})

new Vue({
  store,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
