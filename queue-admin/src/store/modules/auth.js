const url = 'http://localhost:3000/login'

const state = { 
	token: localStorage.getItem('authorization') || '',
  status: '',
  resetPassword: false,
  roles: []
	// hasLoadedOnce: false
}

const getters = {
  token: state => state.token,
  authStatus: state => state.status,
  resetPassword: state => state.resetPassword,
  roles: state => state.roles
}

const actions = {
  AUTH_REQUEST: async ({commit}, credentials) => {
    commit('AUTH_REQUEST')
    try {
      const loginConfirmation = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(credentials),
        headers: {
          'Content-Type': 'application/json',
        } 
      })

      const res = await  loginConfirmation.json()
     
      if (!loginConfirmation.ok) {
        throw Error(res.msg)
      }
      let response = {
        status: loginConfirmation.status,
        ...res
      }
      localStorage.setItem('authorization', `Bearer: ${res.token}`)
      commit('AUTH_SUCCESS', `Bearer: ${res.token}`)
      commit('SET_ROLES', res.roles)
      return response

    } catch (err) {
      commit('AUTH_ERROR', err)
      localStorage.removeItem('authorization')
      return err
    }

  },
  AUTH_LOGOUT: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      commit('AUTH_LOGOUT')
      localStorage.removeItem('authorization')
      resolve()
    })
  },
  RESET_PASSWORD: ({commit}, reset) => {
    commit('RESET_PASSWORD', reset)
  }
}

const mutations = {
  AUTH_REQUEST: (state) => {
    state.status = 'loading'
  },
  AUTH_SUCCESS: (state, token) => {
    state.status = 'success'
    state.token = token
    // state.hasLoadedOnce = true
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  RESET_PASSWORD:(state, reset) => {
    state.resetPassword = reset
  },
  AUTH_ERROR: (state) => {
    state.status = 'error'
    // state.hasLoadedOnce = true
  },
  AUTH_LOGOUT: (state) => {
    state.token = ''
    state.status = ''
    state.roles = []
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}