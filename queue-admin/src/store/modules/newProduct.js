const state = { 
	product: {
    product_name: '',
    desc_short: '',
    desc_long: '',
    product_price: '',
    type_id: 0,
    img: '',
    available: true,

    options: {},
    
  },
  imgInput: [],
  imgObject: {
    imgPathTo: undefined,
    img: {
      type: '',
      data: []
    }
  },

}

const getters = {
  product_name: state => state.product.product_name,
  desc_short: state => state.product.desc_short,
  desc_long: state => state.product.desc_long,
  product_price: state => state.product.product_price,
  type_id: state => state.product.type_id,
  img: state => state.product.img,
  options: state => state.product.options,

  imgObject: state => state.imgObject,
  product: state => state.product
}

const actions = {
  updateField: ({commit}, payload) => {
    commit('updateField', payload)
  },
  updateOptions: ({commit}, payload) => {
    commit('updateOptions', payload)
  },
  updateImg: ({commit}, payload) => {
    commit('updateImg', payload)
  },
  resetProduct: ({commit}) => {
    commit('resetProduct')
  }
}

const mutations = {
  updateField: (state, payload) => {
    state.product[payload.field] = payload.value
    console.log(state.product)
  },
  updateOptions: (state, payload) => {
    state.product.options[payload.option_id] = payload.value
    console.log(state.product.options)
  },
  updateImg: (state, payload) => {
    state[payload.field] = payload.value
    if (payload.field == 'imgObject') {
      state.product.img = payload.value.imgPathTo
    }
  },
  resetProduct: state => {
    state.product = {
      product_name: '',
      desc_short: '',
      desc_long: '',
      product_price: '',
      type_id: 0,
      img: '',
      available: true,
  
      options: {},
      
    }
    state.imgInput = []
    state.imgObject = {
      imgPathTo: undefined,
      img: {
        type: '',
        data: []
      }
    }
  }
  // AUTH_REQUEST: (state) => {
  //   state.status = 'loading'
  // },
  // AUTH_SUCCESS: (state, token) => {
  //   state.status = 'success'
  //   state.token = token
  // },
  // SET_ROLES: (state, roles) => {
  //   state.roles = roles
  // },
  // AUTH_ERROR: (state) => {
  //   state.status = 'error'
  // },
  // AUTH_LOGOUT: (state) => {
  //   state.token = ''
  //   state.status = ''
  //   state.roles = []
	// }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}