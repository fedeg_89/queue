const state = { 
	snackMsg: false,
	color: 'success', // success, warning, error
	text: '',
}

const getters = {
  snackMsg: state => state.snackMsg,
  color: state => state.color,
  text: state => state.text
}

const actions = {
  showSnack: ({commit}, payload) => { // payload = {color: String, text: String}
    commit('showSnack', payload)
	},
	updateSnack: ({commit}, payload) => {
		commit('updateSnack', payload)
	}
}

const mutations = {
  showSnack: (state, payload) => {
    state.color = payload.color
    state.text = payload.text
		state.snackMsg = true
	},
	updateSnack: (state, value) => {
		state.snackMsg = false
		state.snackMsg = value
	}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}