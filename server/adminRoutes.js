const express = require('express')
const admin = express.Router()
const bcrypt = require('bcrypt')
const saltRounds = 10
const multer  = require('multer')
const uuidv1 = require('uuid/v1')
uuidv1();

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `./images/client_${req.authData.client_id}`)
  },
  filename: function (req, file, cb) {
    req.body.nameId = uuidv1()
    cb(null, `${req.body.nameId}.jpg`)
  }
});
const upload = multer({ storage })

const fs = require('fs')
const tinify = require("tinify")
tinify.key = "wFxCXJ3ncLzVz2qbqPCT1jwcq9X9PlJZ"


const mysql = require('mysql2')
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'queue'
})
 
// connection.connect();

// connection.end();

// admin.param('type_id', function(req, res, next, type_id) {
//   if (!type_id) {
//     console.log('nopo')
//     next()
//   } else {
//     console.log(type_id)
//     next()
//   }
// })

function errorHandling(err, req, res, next) {
  res.status(400).json({err, msg: 'algo salió mal!'})
  throw err
}


admin.route('/getProducts').get(function (req, res, next) {
  const clientStatement = `products.client_id = ${req.authData.client_id} AND types.client_id = ${req.authData.client_id}`
  connection.execute(`SELECT products.*, types.type_name FROM products LEFT JOIN types ON products.type_id = types.type_id WHERE ${clientStatement}` , function (error, products) {
    if (error) return next(error)
    for (const product of products) {
      // Converts the img to base64
      if (product.img) {
        try {
          const img = fs.readFileSync(product.img)
          let TYPED_ARRAY = new Uint8Array(img);
          const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
          product.img = STRING_CHAR
        } catch (error) {
          return next(error)
        }
      }
    }
    res.json(products)
  })
})

admin.route('/getTypes').get(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM types WHERE ${clientStatement}`, function (error, types) {
    if (error) return next(error)
    res.json(types)
  })
})



admin.route('/getOneType/:type_id').get( function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM types WHERE type_id = ${req.params.type_id} AND ${clientStatement}`, function (error, types) {
    if (error) return next(error)
    res.json(types)
  })
})


admin.route('/getOptions').get(function (req, res, next) { 
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM options WHERE ${clientStatement}`, function (error, options) {
    if (error) return next(error)
    res.json(options)
  })
})

admin.route('/getOptionsByGroup/:product_id').get(function (req, res, next) { 
  const clientStatement = `options.client_id = ${req.authData.client_id}`
  connection.execute(`SELECT options.* ,products_options.product_id, products_options.selected FROM options LEFT JOIN products_options ON products_options.option_id = options.option_id AND products_options.product_id = ${req.params.product_id} WHERE options.type_id = (SELECT type_id from products where product_id = ${req.params.product_id}) AND ${clientStatement}`, function (error, options) {
    if (error) return next(error)
    res.json(options)
  })
})

admin.route('/getOptionsByType/:type_id').get(function (req, res, next) { 
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM options WHERE type_id = ${req.params.type_id} AND ${clientStatement}`, function (error, options) {
    if (error) return next(error)
    res.json(options)
  })
})

admin.route('/getProductsOptionsForProduct/:product_id').get(function (req, res, next) {
  const clientStatement = `options.client_id = ${req.authData.client_id} AND products_options.client_id = ${req.authData.client_id}` 
  connection.execute(`SELECT options.*, products_options.product_id FROM options LEFT JOIN products_options ON products_options.option_id = options.option_id AND products_options.product_id = ${req.params.product_id} WHERE options.type_id = (SELECT type_id FROM products WHERE product_id = ${req.params.product_id} LIMIT 1) AND ${clientStatement}`, function (error, products_options) {
    if (error) return next(error)
    res.json(products_options)
  })
})

admin.route('/getProductsOptions').get(function (req, res, next) {
  const clientStatement = `products_options.client_id = ${req.authData.client_id} AND products.client_id = ${req.authData.client_id} AND options.client_id = ${req.authData.client_id}` 
  connection.execute(`SELECT products_options.*, products.product_name, options.option_name, options.option_group_name FROM products_options LEFT JOIN products ON products_options.product_id = products.product_id LEFT JOIN options ON products_options.option_id = options.option_id WHERE ${clientStatement}`, function (error, product_options) {
    if (error) return next(error)
    res.json(product_options)
  })
})

admin.route('/getCertainOptions/:optionsIds').get(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}` 
  connection.execute(`SELECT * FROM options WHERE option_id IN (${req.params.optionsIds}) AND ${clientStatement}`, function (error, options) {
    if (error) return next(error)
    res.json(options)
  })
})

admin.route('/getUsers').get(async function (req, res, next) {
  let clientStatement = `client_id = ${req.authData.client_id}`
  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  const [users] = await connection.query(`SELECT user_id, user_name, person_name FROM users WHERE ${clientStatement}`)

  clientStatement = `users_roles.client_id = ${req.authData.client_id}`
  for (let user of users) {
    const [roles] = await connection.query(`SELECT users_roles.role_id, roles.role_name FROM users_roles LEFT JOIN roles on users_roles.role_id = roles.role_id WHERE users_roles.user_id = ${user.user_id} AND ${clientStatement}`)

    user.roles = roles
  }

  res.json(users)
})

admin.route('/getRoles').get(function (req, res, next) {
  // const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM roles WHERE NOT role_id = 0`, function (error, roles) {
    if (error) return next(error)
    res.json(roles)
  })
})

admin.route('/setNewPassword').patch(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
    if (err) throw err
    connection.execute(`UPDATE users SET password = '${hash}', reset_password = 0 WHERE user_id = ${req.body.user_id} AND ${clientStatement}`, function(error, result) {
      if (error) return next(error)
    });
    res.json({res: 'ok'})
  })
})

admin.route('/resetPassword').patch(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  const words = [
    'queue',
    'burger',
    'laproxima',
    'buenintento',
    'saleconfritas',
    'beer',
    'wwjd'
  ]
  const number = Math.floor(Math.random() * (9999 - 1000 + 1) ) + 1000
  const randomWord = Math.floor(Math.random() * (words.length) )
  const newPassword = words[randomWord] + number

  bcrypt.hash(newPassword, saltRounds, function(err, hash) {
    if (err) throw err
    connection.query(`UPDATE users SET password = '${hash}', reset_password = 1 WHERE user_id = ${req.body.user_id} AND ${clientStatement}`)
  })


  res.json({res: 'ok', newPassword})
})

admin.route('/getUserData/:user_id').get(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT user_name, person_name FROM users WHERE user_id = ${req.params.user_id} AND ${clientStatement}`, function (error, user) {
    if (error) return next(error)
    res.json(user[0])
  })
})


admin.route('/updateAvailability').patch(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`UPDATE products SET available = ${req.body.available} WHERE product_id = ${req.body.product_id} AND ${clientStatement}`, function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
  })
})

admin.route('/dispatchOrder').patch(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`UPDATE orders SET dispatched = ${req.body.dispatched} WHERE order_id = ${req.body.order_id} AND ${clientStatement}`, function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
  })
})

admin.route('/insertImg').post(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`UPDATE products SET img = ${req.body.imgName} WHERE product_id = ${req.body.product_id} AND ${clientStatement}`, function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
  })
})

admin.route('/deleteImg').patch(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`UPDATE products SET img = '' WHERE product_id = ${req.body.product_id} AND ${clientStatement}`, function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
  })
})

admin.route('/getTimings').get( function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`SELECT * FROM orders_timing WHERE ${clientStatement}`, function (error, timings) {
    if (error) return next(error)
    res.json(timings)
  })
})

admin.route('/adminConfigs/:field')
  .get(function(req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.execute(`SELECT ${req.params.field} FROM clients WHERE ${clientStatement}`, function (error, [config]) {
      if (error) return next(error)
      res.json(config)
    })
    
  })
  .patch(function(req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    const value = {
      [req.body.field]: req.body.value
    }
    connection.query(`UPDATE clients SET ? WHERE ${clientStatement}`, [value], function(error, result) {
      if (error) return next(error)
      res.json({msg: 'Configuración actualizada', result})
    })
  })
//************* */
admin.route('/getOrders').get(async function (req, res, next) {
  let clientStatement = `client_id = ${req.authData.client_id}` 
  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  try {
    const [orders] = await connection.query(`SELECT * FROM orders WHERE ${clientStatement}`)

    for (let order of orders) {
      clientStatement = `orders_detail.client_id = ${req.authData.client_id} AND products.client_id = ${req.authData.client_id}` 
      const [items] = await connection.query(`SELECT orders_detail.*, products.product_name FROM orders_detail LEFT JOIN products ON orders_detail.product_id = products.product_id WHERE order_id = ${order.order_id} AND ${clientStatement}`)
      
      for (let item of items) {
        clientStatement = `client_id = ${req.authData.client_id}`
        const [options] = await connection.query(`SELECT option_name FROM options WHERE option_id IN (SELECT option_id FROM options_detail WHERE order_detail_id = ${item.order_detail_id}) AND ${clientStatement}`)
        
        item.options = options
      }
      order.items = items
    }

    res.json(orders)
  } catch (error) {
    return next(error)
  }
  
})
//************* */

admin.route('/getItemsInOrder/:items').get(async function (req, res, next) {
  const itemsData = JSON.parse(req.params.items)
  let items = []

  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});

  try {
    for (const singleItem of itemsData) {

      let clientStatement = `client_id = ${req.authData.client_id}`
      const [options] = await connection.execute(`SELECT option_id, option_name, option_price FROM options WHERE option_id IN (${singleItem.options}) AND ${clientStatement}`)
      
      clientStatement = `products.client_id = ${req.authData.client_id} AND types.client_id = ${req.authData.client_id}`
      const [productData] = await connection.execute(`SELECT products.product_name, types.type_id, types.type_name FROM products LEFT JOIN types ON products.type_id = types.type_id WHERE products.product_id = ${singleItem.product_id} AND ${clientStatement}`)
  
  
      let item = {
        product_id: singleItem.product_id,
        product_name: productData[0].product_name,
        quantity: singleItem.quantity,
        order_detail_price: singleItem.order_detail_price,
        type_id: productData[0].type_id,
        type_name: productData[0].type_name,
        order_detail_notes: singleItem.order_detail_notes,
        options
      }
  
      items.push(item)
    }
    res.json(items)

  } catch (error) {
    return next(error)
  }
  
})

//************* */



admin.route('/productCRUD')
  .patch(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.query(`UPDATE products SET ? WHERE product_id = ${req.body.product_id} AND ${clientStatement}`, [req.body], function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })
  .post(function (req, res, next) {
    req.body.client_id = req.authData.client_id
    connection.execute(`SELECT MAX(product_id) FROM products WHERE type_id = ${req.body.type_id} AND ${clientStatement}`, function(error, lastId) {
      if (error) return next(error)
      req.body.product_id = lastId + 1
      connection.query(`INSERT INTO products SET ?`, [req.body], function(error, result) {
        if (error) return next(error)
        res.json({res: 'ok'})
      })
    })
  })
  .delete(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.execute(`DELETE FROM products WHERE product_id = ${req.body.product_id} AND ${clientStatement}`, function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })

admin.route('/typeCRUD')
  .patch(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.query(`UPDATE types SET ? WHERE type_id = ${req.body.type_id} AND ${clientStatement}`, [req.body], function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })
  .post(function (req, res, next) {
    req.body.client_id = req.authData.client_id
    connection.query(`INSERT INTO types SET ?`, [req.body], function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
    })
  })
  .delete(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.execute(`SELECT type_id FROM products WHERE type_id = ${req.body.type_id} AND ${clientStatement}`, function(error, result) {
      if (result) {
        res.json({res: 'in use'})
      } else {
        connection.execute(`DELETE FROM types WHERE type_id = ${req.body.type_id} AND ${clientStatement}`, function(error, confirmation) {
          if (error) return next(error)
          res.json({res: 'ok'})
        })
      }
    })
  })

admin.route('/optionCRUD')
  .patch(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.query(`UPDATE options SET ? WHERE option_id = ${req.body.option_id} AND ${clientStatement}`, [req.body], function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })
  .post(async function (req, res, next) {
    const mysql = require('mysql2/promise');
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'})
    const clientStatement = `client_id = ${req.authData.client_id}`

    req.body.client_id = req.authData.client_id

    if (req.body.newGroup) {
      const [lastGroupId] = await connection.execute(`SELECT MAX(option_group_id) as lastGroupId FROM options WHERE ${clientStatement}`)
      req.body.option_group_id = lastGroupId[0].lastGroupId + 1
      console.log(lastGroupId[0].lastGroupId)
    } else {
      const [groupData] = await connection.execute(`SELECT type_id, option_group_name, option_type FROM options WHERE option_group_id = ${req.body.option_group_id} AND ${clientStatement}`)
      req.body.type_id = groupData[0].type_id
      req.body.option_group_name = groupData[0].option_group_name
      req.body.option_type = groupData[0].option_type
    }

    delete req.body.newGroup

    connection.query(`INSERT INTO options SET ?`, [req.body], function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })
  .delete(function (req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
    connection.execute(`DELETE FROM options WHERE option_id = ${req.body.option_id} AND ${clientStatement}`, function(error, result) {
      if (error) return next(error)
      res.json({res: 'ok'})
    })
  })

admin.route('/usersCRUD')
  .patch(async function (req, res, next) {
    const mysql = require('mysql2/promise');
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'})

    const roles = req.body.roles
    delete req.body.roles
    delete req.body.password
    const user = req.body

    const clientStatement = `client_id = ${req.authData.client_id}`
    const userQuery = await connection.execute(`UPDATE users SET ? WHERE user_id = ${user.user_id} AND ${clientStatement}`, [user])
    const rolesDelete = await connection.execute(`DELETE FROM users_roles WHERE user_id = ${user.user_id} AND ${clientStatement}`) 
    const rolesArray = roles.map(rol => Object.keys(rol))

    for (let role of rolesArray) {
      const rolesCreate = await connection.execute(`INSERT INTO users_roles (client_id, user_id, role_id) VALUES (${req.authData.client_id},${user.user_id},${role})`) 
    }

    res.json({res: 'ok'})
  })
  .post(async function (req, res, next) {
    const mysql = require('mysql2/promise');
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});

    console.log(req.body)

    let roles = []
    // Keeps only the "true" roles
    for (let role in req.body.roles) {
      if (req.body.roles[role]) {
        roles = [...roles, role]
      }
    }
    delete req.body.roles

    const user = req.body
    const hash = await bcrypt.hash(req.body.password, saltRounds)
    user.password = hash
    user.client_id = req.authData.client_id

    const userQuery = await connection.query(`INSERT INTO users SET ?`, [user])

    for (let role of roles) {
      const rolesCreate = await connection.execute(`INSERT INTO users_roles (client_id, user_id, role_id) VALUES (${req.authData.client_id},${userQuery[0].insertId},${role})`) 
    }

    res.json({res: 'ok'})
  })
  .delete(async function (req, res, next) {
    const mysql = require('mysql2/promise');
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});

    const user = req.body

    const clientStatement = `client_id = ${req.authData.client_id}`
    const userQuery = await connection.execute(`DELETE FROM users WHERE user_id = ${user.user_id} AND ${clientStatement}`)
    const rolesDelete = await connection.execute(`DELETE FROM users_roles WHERE user_id = ${user.user_id} AND ${clientStatement}`) 
    
    res.json({res: 'ok'})
  })

admin.route('/timingCRUD')
  .post(async function(req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`

    // Validation
    connection.query(`SELECT max(minutes) as max_minutes FROM orders_timing WHERE ${clientStatement}`, function (error, result) {
      if (error) return next(error)
      if (req.body.minutesRule <= result) {
        res.status(400).json({msg: 'Los minutos no pueden ser menos que para el timing anterior.'})
      } else {
        // The Insert
        req.body.client_id = req.authData.client_id
        connection.query(`INSERT INTO orders_timing SET ?`, [req.body], function(error, insertResult) {
          if (error) return next(error)
          res.status(201).json({msg: 'Timing creado!', insertResult})
        })
      }
    })

  })
  .patch(async function(req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`

    connection.query(`UPDATE orders_timing SET ? WHERE timing_id = ${req.body.timing_id} AND ${clientStatement}`, [req.body], function(error, result) {
      if (error) return next(error)
      res.status(200).json({msg: 'Timing actualizado!', result})
    })
  })
  .delete(async function(req, res, next) {
    const clientStatement = `client_id = ${req.authData.client_id}`
  
    connection.query(`DELETE FROM orders_timing WHERE timing_id = ${req.body.timing_id} AND ${clientStatement}`, function(error, deleteResult) {
      if (error) return next(error)
      res.status(200).json({msg: 'Timing Eliminado!', deleteResult})
    })
  })


admin.route('/createNewProduct').post(async function(req, res, next) {
  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});

  let product = req.body
  product.client_id = req.authData.client_id

  let options = []
  // Keeps only the "true" options
  for (let option in product.options) {
    if (product.options[option]) {
      options = [...options, option]
    }
  }
  delete product.options

  try {
    await connection.beginTransaction() // Will Begin Transaction
    
    const productQuery = await connection.query(`INSERT INTO products SET ?`, [product])

    for (let option of options) {
      const productOption = await connection.execute(`INSERT INTO products_options (client_id, product_id, option_id) VALUES (${req.authData.client_id}, ${productQuery[0].insertId}, ${option})`) 
    }

    await connection.commit();	
    await connection.close();
    res.json({res: 'ok', product: productQuery[0].insertId})
    
  } catch(err) {
    await connection.rollback() // to rollback in case of error 
    await connection.close();
    console.log('err: ', err)
    res.json({err}) 

  }
})


// NOT IMPLEMENTED IN FRONT END
admin.route('/migrateTypes').put(function (req, res, next) {
  const clientStatement = `client_id = ${req.authData.client_id}`
  connection.execute(`UPDATE products SET type_id = ${req.body.to} WHERE type_id = ${req.body.from} AND ${clientStatement}`, function(error, result) {
    if (error) return next(error)
    res.json({res: 'ok'})
  })
})

admin.route('/uploadImg')
  .post(upload.single('img'), async function (req, res, next) {

    if (req.body.oldPath !== 'undefined') {
      fs.unlink(req.body.oldPath, (err) => {
        if (err) return next(err)
        console.log(`${req.body.oldPath} was deleted`)
      })
    }

    const imgPathFrom = `./images/client_${req.authData.client_id}/${req.body.nameId}.jpg`
    const imgPathTo = `./images/client_${req.authData.client_id}/${uuidv1()}.jpg`
    const responseData = {
      name: req.file.originalname,
      imgPathTo
    }

    const file = fs.readFileSync(imgPathFrom) // WORKS!

    const resized = {
      method: "cover",
      width: 400,
      height: 200
    };

    tinify.fromBuffer(file).resize(resized).toBuffer(function(err, resultData) {

      fs.writeFile(imgPathTo, resultData, function(err){
        if(err) return next(err)
        else {
          responseData.img = resultData
          console.log("Successfully written: ", responseData)

          fs.unlink(imgPathFrom, (err) => {
            if (err) return next(err)
            console.log(`${imgPathFrom} was deleted`)
          })
          res.json(responseData)
        }
      });

      if (err) {
        res.json({err: 'error?'})
        return next(err)
      }
    })

  })
  .delete(function (req, res, next) {
    fs.unlink(req.body.oldPath, (err) => {
      if (err) return next(err)
      console.log(`${req.body.oldPath} was deleted`)
      res.json({res: `${req.body.oldPath} was deleted`})
    })
  })


admin.use(errorHandling)

module.exports = admin