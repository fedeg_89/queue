const bcrypt = require('bcrypt')
const saltRounds = 10

// bcrypt.hash('1234', saltRounds, function(err, hash) {
//     if (err) throw err
//     console.log(hash)
//   })

const pass = '$2b$10$7n6tTjlnwhTqHjV1dY5Y0.GJVD86r5MCULtYMo3oJqxs6YW/ol1f6'
bcrypt.compare('1234', pass, function(err, match) {
    console.log(match)
});