const express = require('express')
const order = express.Router()
const jwt = require('jsonwebtoken')

const mysql = require('mysql2');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'queue'
});

// connection.connect();

// connection.end();

order.route('/getProducts').get(function (req, res) {
  const clientStatement = `client_id = ${req.client_id}`
  connection.execute(`SELECT * from products WHERE ${clientStatement}`, function (error, products) {
    if (error) throw error;
    res.json(products)
  })
})

order.route('/getSingleProduct/:product_id').get(function (req, res) {
  const clientStatement = `products.client_id = ${req.client_id}`
  connection.execute(`SELECT products.product_name, products.desc_short, products.product_price, types.type_name, types.type_icon FROM products LEFT JOIN types ON products.type_id = types.type_id WHERE product_id = ${req.params.product_id} AND ${clientStatement}`, function (error, product) {
    if (error) throw error;
    res.json(product)
  })
})

order.route('/getTypes').get(function (req, res) {
  const clientStatement = `client_id = ${req.client_id}`
  connection.execute(`SELECT * from types WHERE ${clientStatement}`, function (error, types) {
    if (error) throw error;
    res.json(types)
  })
})

order.route('/getOptions/:product_id').get(function (req, res) {
  const clientStatement = `options.client_id = ${req.client_id}`
  const product_id = req.params.product_id
  connection.execute(`SELECT options.*, products_options.selected FROM options LEFT JOIN products_options ON options.option_id = products_options.option_id WHERE options.option_id IN (SELECT option_id FROM products_options WHERE product_id = ${product_id}) AND products_options.product_id = ${product_id} AND ${clientStatement}`, function (error, options) {
    if (error) throw error;
    res.json(options)
  })
})

order.route('/getItemsInOrder/:items').get(async function (req, res) {
  const itemsData = JSON.parse(req.params.items)
  let items = []
  
  // res.json(itemsData)
  
  for (const singleItem of itemsData) {
    // await Promise.all(itemsData.forEach(async singleItem => {
    const mysql = require('mysql2/promise');
    const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});

    let clientStatement = `client_id = ${req.client_id}`
    const [options] = await connection.execute(`SELECT option_id, option_name, option_price FROM options WHERE option_id IN (${singleItem.options}) AND ${clientStatement}`)
    clientStatement = `products.client_id = ${req.client_id}`
    const [productData] = await connection.execute(`SELECT products.product_name, types.type_id, types.type_name FROM products LEFT JOIN types ON products.type_id = types.type_id WHERE products.product_id = ${singleItem.product_id} AND ${clientStatement}`)


    let item = {
      product_id: singleItem.product_id,
      product_name: productData[0].product_name,
      quantity: singleItem.quantity,
      order_detail_price: singleItem.order_detail_price,
      type_id: productData[0].type_id,
      type_name: productData[0].type_name,
      order_detail_notes: singleItem.order_detail_notes,
      options
    }

    items.push(item)
  }

  res.json(items)
})

order.route('/confirmOrder').post(async function (req, res) {
  const body = req.body
  const order = {
    client_id: req.body.client_id,
    client: body.client,
    order_date: new Date().toLocaleDateString(),
    order_time: new Date().toLocaleTimeString(),
    deliver_time: new Date (new Date().setMinutes(new Date().getMinutes() + 15)).toLocaleTimeString(), // new Date() + 15 min
    order_price: body.order_price,
    dispatched: false,
    paid: false,
    order_notes: body.order_notes,
  }

  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  const orderResult = await connection.query('INSERT INTO orders SET ?', [order])
  order.order_id = orderResult[0].insertId
  
  for (let item of body.items) {
    item.order_id = await orderResult[0].insertId
    const options = item.options
    delete item.options
    delete item.cartId // this is only for cart front management
    let order_detailResult = await connection.query(`INSERT INTO orders_detail SET ?`, [item])
    
    for (let option of options) {
      let optionDetail = await {
        order_detail_id: order_detailResult[0].insertId,
        option_id: option
      }
      let option_detailResult = await connection.query(`INSERT INTO options_detail SET ?`, [optionDetail])
    }
  }

  // Send data to Queue admin
  const orderEmitter = require('./server').orderEmitter
  order.items = body.items
  orderEmitter.emit('newOrder', order)

  // Give token
  let orders = [orderResult[0].insertId]

  const bearerHeader = req.headers['user']
  if (bearerHeader !== 'null') { // el header 'user' llega siempre aunque sea null (y llega string)
    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]
    const oldOrders = jwt.decode(bearerToken).orders
    orders = [oldOrders, ...orders]
  }

  
  
  jwt.sign({orders}, 'secretkey', { expiresIn: '6h' }, (err, token) => {
    res.json({res: 'Success', order_id: orderResult[0].insertId, deliver_time: order.deliver_time, token})
  });

})

module.exports = order