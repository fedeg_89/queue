const EventEmitter = require('events');
class OrderEmitter extends EventEmitter {}
const orderEmitter = new OrderEmitter();
// ***** Admin Socket ***** \\

const adminNamespace = io.of('/adminNamespace');

orderEmitter.on('newOrder', (order, items) => {
	console.log(order)
	console.log(items)
	adminNamespace.emit('addOrder', order)
})

adminNamespace.on('connection', function(socket){
	console.log('admin connected');
	
	// socket.on('giveMeAnOrder', function(){
	// 	console.log("I'll give you an order")
	// 	// io.emit('addOrder', orders.splice(0,1) );
	// });
})

module.exports.orderEmitter = orderEmitter

// ***** Users Socket ***** \\
const usersNamespace = io.of('/usersNamespace');

orderEmitter.on('orderReady', order_id => {
	console.log(order_id)
	adminNamespace.emit('orderReady', order_id)
})

usersNamespace.on('connection', function(socket){
	console.log('user connected');
	
	// socket.on('giveMeAnOrder', function(){
	// 	console.log("I'll give you an order")
	// });
})


// module.exports.orderSocket = orderSocket
// module.exports.orderEmitter = orderEmitter

