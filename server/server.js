const express = require('express')
const app = express()
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const path = require('path');
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const saltRounds = 10
app.use(cors())

// Sends static files  from the public path directory
app.use(express.static(path.join(__dirname, '/public')))
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


// ========================== SOCKETS ========================== \\

// const orderSocket = require('./orderSocket').orderSocket(io)  
const EventEmitter = require('events');
class OrderEmitter extends EventEmitter {}
const orderEmitter = new OrderEmitter();
// ***** Admin Socket ***** \\

const adminNamespace = io.of('/adminNamespace');

orderEmitter.on('newOrder', (order) => {
	adminNamespace.emit('addOrder', order)
})

adminNamespace.on('connection', function(socket){
	console.log('admin connected');
	
	// socket.on('giveMeAnOrder', function(){
	// 	console.log("I'll give you an order")
	// 	// io.emit('addOrder', orders.splice(0,1) );
	// });
})

module.exports.orderEmitter = orderEmitter

// ***** Users Socket ***** \\
const usersNamespace = io.of('/usersNamespace');

orderEmitter.on('orderReady', order_id => {
	console.log(order_id)
	adminNamespace.emit('orderReady', order_id)
})

usersNamespace.on('connection', function(socket){
	console.log('user connected');
	
	// socket.on('giveMeAnOrder', function(){
	// 	console.log("I'll give you an order")
	// });
})

// ========================== FIN SOCKETS ========================== \\

// SDK de Mercado Pago
const mercadopago = require ('mercadopago');

// Agrega credenciales
mercadopago.configure({
  sandbox: true,
  access_token: 'APP_USR-4210140200905062-011615-479fe7ab89fed1dc6eb3b919fe4625f2-117521967'
});

app.post('/mercadoPago', (req, res) => {
  // Crea un objeto de preferencia
let preference = {
  items: [
    {
      title: 'Mi producto',
      unit_price: 100,
      quantity: 1,
    }
  ]
};

mercadopago.preferences.create(preference)
  .then(function(response){
  // Este valor reemplazará el string "$$init_point$$" en tu HTML
    console.log(response)// global.init_point = response.body.init_point;
    res.json(response)
  }).catch(function(error){
    console.log(error);
    res.json(error)
  });
})

app.post('/login', async function (req, res) {
  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  const [userData] = await connection.execute(`SELECT * FROM users WHERE user_name = ?`, [req.body.user_name])

  if (!userData[0]) {
    res.status(400).json({msg: 'El usuario no existe'})
  } else {
    const match = await bcrypt.compare(req.body.password, userData[0].password);
    if (!match) {
      res.status(400).json({msg: 'La contraseña es incorrecta'})
    } else {
      const client_id = userData[0].client_id
      const user_id = userData[0].user_id
      const reset_password = userData[0].reset_password
      const [rolesIds] = await connection.query(`SELECT role_id FROM users_roles WHERE user_id = ${user_id}`)
      const roles = rolesIds.map(role => role.role_id)
      jwt.sign({client_id, roles, user_id, reset_password}, 'secretkey', { expiresIn: '10h' }, (err, token) => {
        res.json({token, roles, user_id})
      })
    }
  }
})

const orderRoutes = require('./orderRoutes')
app.use('/api/order',verifyClient, orderRoutes)

const userRoutes = require('./userRoutes')
app.use('/api/user', userRoutes)

const adminRoutes = require('./adminRoutes')
app.use('/api/admin',verifyToken, adminRoutes)


app.get('/api/auth', verifyToken, (req, res) => {
  res.json({msg: 'ok', authData: req.authData})
})

app.get('/api/userAuth', (req, res) => {
  const bearerHeader = req.headers['user']
  if (bearerHeader) {
    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'secretkey', (err, authData) => {
      err ? res.status(401).json({msg: 'old'}) : res.json({msg: 'ok', roles: authData.roles})
    });
  } else {
    // Forbidden
    console.log('no token')
    res.status(401).json({msg: 'no'})
  }
})

app.get('/api/clientCheck/:queue_domain', async (req, res) => {
  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  try {
    const [result] = await connection.execute(`SELECT client_id FROM clients WHERE queue_domain = ?`, [req.params.queue_domain])
    if (result == false) {
      res.status(404).json({msg: 'no client'})
    } else {
      jwt.sign({client_id: result[0].client_id}, 'secretkey', { expiresIn: '6h' }, (err, token) => {
        if (err) console.log(err)
        
        res.json({token})
      })
    }
  } catch (error) {
    res.json(error)
  }
})

app.get('/:client_queue_domain', async (req, res) => {
  

  res.sendFile(path.join(__dirname, '../public/index.html'))
  // jwt.sign({client_id}, 'secretkey', { expiresIn: '6h' }, (err, token) => {
  //   if (err) res.json(err)

  //   res.redirect('/')
  // })
})

// app.get('/', (req, res) => {
//   const filePath = path.join(__dirname, '../public/index.html')
//   const options = {}
//   console.log('cookies: ', req.cookies.Client_id)

//   if (req.cookies.Client_id) {
//     options.headers = {
//       'Client_id': req.cookies.Client_id
//     }
//     res.clearCookie('Client_id')
//   }
//   res.sendFile(filePath, options)
// })

app.get('*', (req, res) => {
  res.status(404).json({msg: 'Página no encontrada'})
})

app.all('*', (req, res) => {
  res.status(404).json({msg: 'API not found. Check the params'})
})

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'))
})


function verifyToken(req, res, next) {
  console.log(req.headers['authorization'])
  const bearerHeader = req.headers['authorization']
  if (bearerHeader) {
    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'secretkey', (err, authData) => {
      if (err) {
        res.status(401).json({msg: 'old', err})
      } else {
        req.authData = authData
        // req.client_id = authData.client_id
        next()
      } 
    });
  } else {
    // Forbidden
    console.log('no token')
    res.status(401).json({msg: 'no'})
  }
}

function verifyClient(req, res, next) {
  const bearerHeader = req.headers['client_id']
  console.log(req.headers)
  if (bearerHeader) {
    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'secretkey', (err, token) => {
      if (err) {
        res.status(401).json({msg: 'old', err})
      } else {
        req.client_id = token.client_id
        console.log('req.client_id: ', req.client_id)
        next()
      } 
    });
  } else {
    // Forbidden
    console.log('no token')
    res.status(401).json({msg: 'no'})
  }
}





const port = 3000
http.listen(port, () => console.log(`Queue listening on port ${port}!`))