const express = require('express')
const user = express.Router()
const jwt = require('jsonwebtoken')

const mysql = require('mysql2');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'queue'
});
 
// connection.connect();

// connection.end();

user.route('/getUserOrders/:token').get(function (req, res) {
  
  // const bearer = req.headers['user'].split(' ')
  const bearer = req.params.token.split(' ')
  const bearerToken = bearer[1]
  const orders = jwt.verify(bearerToken, 'secretkey').orders

  connection.execute(`SELECT * from orders WHERE order_id IN (${orders})`, function (error, orders) {
    if (error) throw error;
    console.log(orders)
    res.json(orders)
  })
})

user.route('/getOrderDetail/:order_id').get(async function (req, res) {

  const mysql = require('mysql2/promise');
  const connection = await mysql.createConnection({host:'localhost', user: 'root', database: 'queue'});
  
  const [items] = await connection.query('SELECT * FROM orders_detail WHERE order_id = ?', [req.params.order_id])
  
  for (let item of items) {
    const [options] = await connection.query(`SELECT option_id FROM options_detail WHERE order_detail_id = ${item.order_detail_id}`)
    
    item.options = Object.keys(options)
  }

  res.json(items)
})

/*[
  {
    cartId: 0,
    options: [1,3,5],
    order_detail_notes: ""
    order_detail_price: 220
    product_id: 1
    quantity: 1
  },
  {
    cartId: 0,
    options: [1,3,5],
    order_detail_notes: ""
    order_detail_price: 220
    product_id: 1
    quantity: 1
  },
]*/


/*

*/

module.exports = user