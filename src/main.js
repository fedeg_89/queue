import Vue from 'vue'
import App from './App.vue'
import store from './store'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.config.productionTip = false

import HomeComponent from './homeModule/homeComponent'
import MenuComponent from './menuModule/menuComponent'
import ProductDetail from './productDetailModule/productDetail'
import OrderComponent from './orderModule/orderComponent'
import UserDashboard from './userModule/userDashboard'

// https://blog.sqreen.com/authentication-best-practices-vue/
const url= 'http://localhost:3000/api'
const routes = [
  {path: '/', component: HomeComponent},
  {
    path: '/:company', component: MenuComponent,
    beforeEnter: async function(to, from, next) {
      const clientData = await fetch(`${url}/clientCheck/${to.params.company}`)
      const client = await clientData.json()
      console.log(client)
      if (!client.token) {
        next('/')
      } else {
        
        store.dispatch('setClient_id', `Bearer: ${client.token}` )
        next()
      }
    },
    children: [

      // {path: '/menu', component: MenuComponent},
      {path: '/product/:id', component: ProductDetail},
      {path: '/order', name: 'order', component: OrderComponent, props: true},
      {
        path: '/user',
        component: UserDashboard,
        beforeEnter: async function(to, from, next) {
          const authData = await fetch(`${url}/userAuth`, {headers: new Headers([ ['user', localStorage.getItem('user')]])})
          const auth = await authData.json()
          if (auth.msg == 'ok') {
            next()
          } else {
            // localStorage.removeItem('user')
            store.dispatch('removeToken')
            next('/')
          }
        }
      },
    ]
  },
  
  {path: '*', redirect: '/'},
]

const router = new VueRouter({
  routes
})

new Vue({
  store,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
