const state = {
	items: [],
	token: localStorage.getItem('user') || '',
	client_id: localStorage.getItem('Client_id') || ''
}
  
const getters = {
	orders: state => state.orders,
	items: state => state.items,
	token: state => state.token,
	client_id: state => state.client_id
}

const actions = {
	addItem({ commit }, payload) {
		commit('addItem', payload)
		/*
			payload = {
			}
		*/
	},
	deleteItem({ commit }, cartId) {
		commit('deleteItem', cartId)
		
	},
	resetCart({commit}) {
		commit('resetCart')
	},

	setToken({commit}, token) {
		commit('AUTH_SUCCESS', token)
	},
	removeToken({ commit }) {
		commit('AUTH_REMOVETOKEN')
	},

	setClient_id({commit}, client_id) {
		commit('SET_CLIENT_ID', client_id)
	},
	removeClient_id({ commit }) {
		commit('REMOVE_CLIENT_ID')
	}
}

const mutations= {
	addItem: (state, payload) => {
		payload.cartId = state.currentCartId
		state.currentCartId ++
		state.items.push(payload)
	},
	deleteItem: (state, cartId) => {
		const index = state.items.findIndex(item => item.cartId == cartId)
		state.items.splice(index, 1)
	},
	resetCart: (state) => {
		state.items = [],
		state.currentCartId = 0
		state.client
	},
	AUTH_SUCCESS: (state, token) => {
		localStorage.setItem('user', `Bearer: ${token}`)
		state.token = token
	},
	AUTH_REMOVETOKEN: (state) => {
		localStorage.removeItem('user')
		state.token = ''
	},
	SET_CLIENT_ID: (state, client_id) => {
		localStorage.setItem('Client_id', `Bearer: ${client_id}`)
		state.client_id = client_id
	},
	REMOVE_CLIENT_ID: (state) => {
		localStorage.removeItem('Client_id')
		state.client_id = ''
	}
}
  
export default {
	state,
	getters,
	actions,
	mutations
}